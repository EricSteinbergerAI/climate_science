import 'package:firebase_storage/firebase_storage.dart';

class PrimaryProgress {
  PrimaryProgress();

  StorageReference getStorageRef() {
    return FirebaseStorage.instance.ref().child(getProgressDir());
  }

  Future storeToDisk() async {
    throw UnimplementedError;
  }

  Future loadFromDisk() async {
    throw UnimplementedError;
  }

  Future sync() async {
    throw UnimplementedError;
  }

  String getProgressDir() {
    throw UnimplementedError;
  }
}
