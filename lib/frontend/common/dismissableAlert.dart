import 'package:climate_science/frontend/login/theme.dart';
import 'package:flutter/material.dart';

import 'Constants.dart';

AlertDialog getDismissableAlert({
  context,
  formText,
  dismissText = "Okay",
}) {
  return getGeneralAlert(
    context: context,
    body: Text(
      formText,
      style: ThemeHolder.formTextStyle,
      maxLines: null,
    ),
    dismissText: dismissText,
    onTap: () {
      Navigator.pop(context);
    }
  );
}

AlertDialog getGeneralAlert({
  context,
  Widget body,
  Function onTap,
  String dismissText = "Okay",
}) {
  return AlertDialog(
    shape: const RoundedRectangleBorder(
        borderRadius: const BorderRadius.all(const Radius.circular(32.0))),
    contentPadding: const EdgeInsets.all(0),
    content: Container(
      decoration: const BoxDecoration(
        borderRadius: const BorderRadius.all(const Radius.circular(32.0)),
        gradient: const LinearGradient(
          colors: const [Color(0xFF8185E2), Constants.buttonColor],
          begin: const Alignment(-1.0, -1.0),
          end: const Alignment(1.0, 1.0),
        ),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.fromLTRB(25, 30, 25, 30),
            child: body,
          ),
          _getDismissAlertButton(
            context: context,
            onTap: onTap,
            dismissText: dismissText,
          ),
        ],
      ),
    ),
  );
}

Widget _getDismissAlertButton({
  context,
  Function onTap,
  String dismissText = "Okay",
}) {
  return InkWell(
    onTap: () async {
      if (onTap != null) {
        await onTap();
      }
    },
    borderRadius: const BorderRadius.only(
        bottomLeft: const Radius.circular(32.0),
        bottomRight: const Radius.circular(32.0)),
    child: Container(
      padding: const EdgeInsets.only(top: 20.0, bottom: 20.0),
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: const BorderRadius.only(
            bottomLeft: const Radius.circular(32.0),
            bottomRight: const Radius.circular(32.0)),
      ),
      child: Text(
        dismissText,
        style: const TextStyle(
          color: Constants.buttonColor,
          fontFamily: Constants.fontName,
          fontWeight: FontWeight.w500,
          fontSize: 18,
        ),
        textAlign: TextAlign.center,
      ),
    ),
  );
}
