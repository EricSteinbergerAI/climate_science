import 'package:flutter/material.dart';

import 'Constants.dart';

Widget getCircularProgress({strokeWidth: 5.0}) {
  return CircularProgressIndicator( strokeWidth: strokeWidth,
      valueColor: AlwaysStoppedAnimation<Color>(Constants.buttonColor));
}
