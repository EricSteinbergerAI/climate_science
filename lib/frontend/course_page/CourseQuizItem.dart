import 'package:climate_science/backend/content/CourseQuiz.dart';
import 'package:climate_science/frontend/common/Constants.dart';
import 'package:climate_science/frontend/common/progressbars.dart';
import 'package:climate_science/frontend/common/try_download.dart';
import 'package:climate_science/frontend/quiz_page/QuizWidget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CourseQuizItem extends StatefulWidget {
  final CourseQuiz courseQuiz;

  CourseQuizItem(this.courseQuiz);

  @override
  _CourseQuizItemState createState() => _CourseQuizItemState();
}

class _CourseQuizItemState extends State<CourseQuizItem> {
  @override
  Widget build(BuildContext context) {
    Function maybeGoToQuiz = () async {
      if (widget.courseQuiz.levelOfDetail
              .getChapters()
              .any((e) => e.isDownloading) ||
          widget.courseQuiz.levelOfDetail.getQuiz().isDownloading) {
        return;
      }
      final success = await Downloading.doNothingIfLoadedElseUpdateAndLoad(
          context: context,
          downloadable: widget.courseQuiz,
          setStateCallback: () => setState(() {}));
      if (success) {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => QuizWidget(widget.courseQuiz)),
        );
      }
    };
    return InkWell(
      onTap: maybeGoToQuiz,
      child: Padding(
        padding: const EdgeInsets.only(top: 2, bottom: 2),
        child: Container(
          height: 65,
          width: double.infinity,
          padding:
              const EdgeInsets.only(right: 3.0, top: 5, bottom: 5, left: 00.0),
          child: Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                child: widget.courseQuiz.isDownloading
                    ? Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: getCircularProgress())
                    : Container(
                        width: 5,
                      ),
              ),
              Expanded(
                child: const Text(
                  "Final Quiz",
                  style: Constants.coursePageChapterNameTextStyle,
                  textAlign: TextAlign.left,
                ),
              ),
              IconButton(
                onPressed: maybeGoToQuiz,
                padding: const EdgeInsets.all(0),
                icon: widget.courseQuiz.isCompleted()
                    ? const Icon(
                        Icons.check_circle,
                        color: Constants.grey,
                        size: 23.0,
                      )
                    : const Icon(
                        Icons.keyboard_arrow_right,
                        color: Constants.grey,
                        size: 23.0,
                      ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
