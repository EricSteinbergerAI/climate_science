import 'package:climate_science/backend/authentication.dart';
import 'package:climate_science/backend/content/Chapter.dart';
import 'package:climate_science/backend/content/Course.dart';
import 'package:climate_science/backend/content/LevelOfDetail.dart';
import 'package:climate_science/frontend/common/Constants.dart';
import 'package:climate_science/frontend/common/progressbars.dart';
import 'package:climate_science/frontend/common/try_download.dart';
import 'package:climate_science/frontend/course_page/CourseQuizItem.dart';
import 'package:flutter/material.dart';

import 'ChapterItem.dart';

class CoursePageWidget extends StatefulWidget {
  final Course course;
  Auth auth;

  CoursePageWidget({this.course, this.auth});

  @override
  _CoursePageWidgetState createState() => _CoursePageWidgetState();
}

class _CoursePageWidgetState extends State<CoursePageWidget> {
  LevelOfDetail _currLevelOfDetail;

  @override
  initState() {
    super.initState();
    _currLevelOfDetail = widget.course.getBasic();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
        //`true` if you want Flutter to automatically add Back Button when needed,
        //or `false` if you want to force your own back button every where
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () => Navigator.pop(context, false),
        ),
        title: Text(
          widget.course.getTitle(),
          style: Constants.appBarTextStyle,
        ),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Container(
          color: Constants.bg1,
          padding: const EdgeInsets.fromLTRB(0, 0, 0, 80),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 45, 0, 15),
                child: Center(
                  child: Text(
                    widget.course.getTitle(),
                    style: Constants.coursePageCourseTitleTextStyle,
                    textAlign: TextAlign.center,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                child: Text(
                  '${widget.course.getSummary()}' + '\n',
                  style: Constants.articleBodyTextStyle,
                  textAlign: TextAlign.justify,
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 10, 0, 15),
                child: _buildButtons(),
              ),
              Align(
                alignment: Alignment.centerRight,
                child: Padding(
                  padding: EdgeInsets.only(bottom: 12, right: 27, left: 27),
                  child: _getDownloadingWidget(),
                ),
              ),
              Container(
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: const BorderRadius.all(Radius.circular(17)),
                  boxShadow: [
                    const BoxShadow(
                      color: Constants.shadow,
                      blurRadius: 15,
                      offset: Offset.zero,
                    )
                  ],
                ),
                margin: const EdgeInsets.fromLTRB(27, 0, 27, 0),
                child: Column(
                  children: <Widget>[
                    Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: _currLevelOfDetail
                              .getChapters()
                              .map((chapter) =>
                                  _getChapterItem(chapter: chapter))
                              .toList(),
                        ),
                        decoration: const BoxDecoration(
                          color: Colors.white,
                          borderRadius: const BorderRadius.only(
                            topLeft: const Radius.circular(17),
                            topRight: const Radius.circular(17),
                          ),
                        )),
                    Container(
                      decoration: const BoxDecoration(
                        gradient: const LinearGradient(
                          begin: const Alignment(-1.0, -1.0),
                          end: const Alignment(1.0, 1.0),
                          colors: [
                            Constants.gradient2R,
                            Constants.gradient2L,
                          ],
                        ),
                        borderRadius: const BorderRadius.only(
                          bottomLeft: const Radius.circular(17),
                          bottomRight: const Radius.circular(17),
                        ),
                      ),
                      child: CourseQuizItem(
                        _currLevelOfDetail.getQuiz(),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  bool _anyIsDownloading() {
    return widget.course.getLODs().values.any((lod) =>
        lod.getChapters().any((e) => e.isDownloading) ||
        _currLevelOfDetail.getQuiz().isDownloading);
  }

  bool _somethingHasNotBeenDownloaded() {
    return widget.course.getLODs().values.any((lod) =>
        lod.getChapters().any((e) => !e.anyVersionDownloaded()) ||
        !_currLevelOfDetail.getQuiz().anyVersionDownloaded());
  }

  Widget _getDownloadingWidget() {
    return Container(
      height: 45,
      child: _anyIsDownloading()
          ? Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(right: 8, left: 8),
                  child: SizedBox(
                    width: 18,
                    height: 18,
                    child: getCircularProgress(strokeWidth: 3.0),
                  ),
                ),
                const Text(
                  "Downloading...",
                  style: Constants.coursePageDownloadTextStyle,
                ),
              ],
            )
          : (_somethingHasNotBeenDownloaded()
              ? GestureDetector(
                  onTap: () async {
                    final other = _currLevelOfDetail.course
                        .getLODs()[_currLevelOfDetail.getOtherStr()];
                    await Downloading.batchDownload(
                      context: context,
                      downloadables: [
                        ..._currLevelOfDetail.getChapters(),
                        _currLevelOfDetail.getQuiz(),
                        ...other.getChapters(),
                        other.getQuiz(),
                      ],
                      setStateCallback: () => setState(() {}),
                    );
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      const Padding(
                        padding: const EdgeInsets.only(right: 4, left: 4),
                        child: const Icon(
                          Icons.file_download,
                          color: Constants.lightText,
                          size: 23.0,
                        ),
                      ),
                      const Text(
                        "Save to phone",
                        style: Constants.coursePageDownloadTextStyle,
                      ),
                    ],
                  ),
                )
              : Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    const Padding(
                      padding: const EdgeInsets.only(right: 4, left: 4),
                      child: const Icon(
                        Icons.offline_pin,
                        color: Constants.lightGrey,
                        size: 23.0,
                      ),
                    ),
                    const Text(
                      "Available offline",
                      style: Constants.coursePageDownloadTextStyle,
                    ),
                  ],
                )),
    );
  }

  Widget _getChapterItem({Chapter chapter}) {
    if (_currLevelOfDetail.getChapters().first == chapter) {
      return Column(
        children: <Widget>[
          Container(
            decoration: const BoxDecoration(
              borderRadius: const BorderRadius.only(
                topLeft: const Radius.circular(17),
                topRight: const Radius.circular(17),
              ),
            ),
            child: ChapterItem(
              chapter: chapter,
              auth: widget.auth,
            ),
          ),
          const Divider(
            indent: 15,
            endIndent: 15,
            color: Constants.lightGrey,
            height: 0,
          ),
        ],
      );
    }

    return Column(
      children: <Widget>[
        ChapterItem(
          chapter: chapter,
          auth: widget.auth,
        ),
        const Divider(
          indent: 15,
          endIndent: 15,
          color: Constants.lightGrey,
          height: 0,
        ),
      ],
    );
  }

  Widget _buildButtons() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        GestureDetector(
          onTap: () {
            setState(() {
              _currLevelOfDetail = widget.course.getBasic();
            });
          },
          child: _currLevelOfDetail.asStr() == LevelOfDetail.BASICS
              ? const Text(
                  LevelOfDetail.BASICS_DISPLAY,
                  style: const TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 17,
                    color: Constants.nearlyBlack,
                  ),
                )
              : const Text(
                  LevelOfDetail.BASICS_DISPLAY,
                  style: const TextStyle(
                    fontWeight: FontWeight.normal,
                    fontSize: 17,
                    color: Constants.lightText,
                  ),
                ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: Switch(
            activeColor: Constants.nearlyBlack,
            activeTrackColor: Constants.nearlyBlack40op,
            inactiveThumbColor: Constants.nearlyBlack,
            inactiveTrackColor: Constants.nearlyBlack40op,
            onChanged: (value) {
              setState(() {
                _currLevelOfDetail =
                    _currLevelOfDetail.asStr() == LevelOfDetail.BASICS
                        ? widget.course.getAdvanced()
                        : widget.course.getBasic();
              });
            },
            value: _currLevelOfDetail.asStr() == LevelOfDetail.ADVANCED,
          ),
        ),
        GestureDetector(
          onTap: () {
            setState(() {
              _currLevelOfDetail = widget.course.getAdvanced();
            });
          },
          child: _currLevelOfDetail.asStr() == LevelOfDetail.ADVANCED
              ? const Text(
                  LevelOfDetail.ADVANCED_DISPLAY,
                  style: const TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 17,
                    color: Constants.nearlyBlack,
                  ),
                )
              : const Text(
                  LevelOfDetail.ADVANCED_DISPLAY,
                  style: const TextStyle(
                    fontWeight: FontWeight.normal,
                    fontSize: 17,
                    color: Constants.lightText,
                  ),
                ),
        ),
      ],
    );
  }
}
