import 'package:climate_science/backend/authentication.dart';
import 'package:climate_science/frontend/root_page/RootPageWidget.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';

void main() {
  // Pass all uncaught errors from the framework to Crashlytics.
  FlutterError.onError = Crashlytics.instance.recordFlutterError;
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Climate Science',
      theme: ThemeData(
        primaryColor: Colors.white,
      ),
      home: RootPage(auth: Auth()),
    );
  }
}
