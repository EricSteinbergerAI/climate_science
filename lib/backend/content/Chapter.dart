import 'dart:async';
import 'dart:convert';
import 'dart:io' as io;

import 'package:climate_science/backend/content/_DownloadableProgressableContent.dart';
import 'package:climate_science/backend/content/modules/article/challenges/ChallengeModuleBase.dart';
import 'package:climate_science/backend/content/progress/ChapterProgress.dart';
import 'package:flutter/material.dart';

import 'Course.dart';
import 'LevelOfDetail.dart';
import 'Topic.dart';
import 'modules/article/BarChartModule.dart';
import 'modules/article/BulletpointModule.dart';
import 'modules/article/ContentModuleBase.dart';
import 'modules/article/GraphModule.dart';
import 'modules/article/ImageModule.dart';
import 'modules/article/OpenProblemsModule.dart';
import 'modules/article/ParagraphModule.dart';
import 'modules/article/Reference.dart';
import 'modules/article/TitleModule.dart';
import 'modules/article/VideoModule.dart';
import 'modules/article/challenges/CheckboxChallengeModule.dart';
import 'modules/article/challenges/RadioBtnChallengeModule.dart';

class Chapter extends DownloadableProgressableContent {
  String _chapterId;
  String _chapterName;
  List<ContentModuleBase> moduleList;
  ChapterProgress _chapterProgress;

  Map<String, Reference> references;

  Image chapterCoverImg;

  String appSupportDir;

  Topic topic;
  Course course;
  LevelOfDetail levelOfDetail;

  Chapter({
    String chapterId,
    String chapterName,
    this.appSupportDir,
  }) : super("chapter") {
    this._chapterId = chapterId;
    this._chapterName = chapterName;

    this._chapterProgress = ChapterProgress(
      chapter: this,
    );
  }

  String getDir() {
    return '$appSupportDir/content/${course.getCourseID()}/$_chapterId';
  }

  String getRemoteDir() {
    return '${course.getCourseID()}/$_chapterId';
  }

  String getDocument() {
    return '${course.getCourseID()}/$_chapterId-chapter';
  }

  bool isLoaded() {
    return moduleList != null;
  }

  Future<void> loadFromDisk() async {
    final jsonFile = io.File('${getDir()}/$jsonContentFileName.json');

    // ------------------------- Load Modules -------------------------
    List<ContentModuleBase> tempModuleList = List();
    Map<String, dynamic> chapterDict = json.decode(jsonFile.readAsStringSync());
    // build all modules
    for (var moduleDict in chapterDict["modules"]) {
      String moduleType = moduleDict["moduleType"];
      // if image, download
      if (moduleType == "paragraph") {
        tempModuleList.add(ParagraphModule(
          content: '${moduleDict["content"]}',
        ));
      } else if (moduleType == "title") {
        tempModuleList.add(TitleModule(
          text: moduleDict["content"],
        ));
      } else if (moduleType == "bulletpoints") {
        tempModuleList.add(BulletpointModule(
          points: moduleDict["content"].cast<String>(),
        ));
      } else if (moduleType == "image") {
        tempModuleList.add(ImageModule(
          imgPath: '${getDir()}/${moduleDict["content"]}',
        ));
      } else if (moduleType == "video") {
        tempModuleList.add(VideoModule(
          videoURL: moduleDict["content"],
        ));
      } else if (moduleType == "barchart") {
        tempModuleList.add(BarChartModule(
          content: moduleDict["content"].cast<Map<String, dynamic>>(),
          yAxisLabel: moduleDict["y-axis"],
        ));
      } else if (moduleType == "graph") {
        tempModuleList.add(GraphModule(
          x: moduleDict["x"].cast<num>(),
          y: moduleDict["y"].cast<num>(),
          xAxisLabel: moduleDict["x-axis"],
          xAxisType: moduleDict["x-axis-type"],
          yAxisLabel: moduleDict["y-axis"],
        ));
      } else if (moduleType == "radiobuttonChallenge") {
        tempModuleList.add(RadioBtnChallengeModule(
          question: moduleDict["question"],
          correctAnswer: moduleDict["correctAnswer"],
          options: moduleDict["options"].cast<String>(),
          moduleId: moduleDict["moduleId"],
        ));
      } else if (moduleType == "checkboxChallenge") {
        tempModuleList.add(CheckboxChallengeModule(
          question: moduleDict["question"],
          correctAnswers: moduleDict["correctAnswers"].cast<String>(),
          options: moduleDict["options"].cast<String>(),
          moduleId: moduleDict["moduleId"],
        ));
      } else if (moduleType == "openProblems") {
        tempModuleList.add(OpenProblemsModule(
          title: moduleDict["title"],
          description: moduleDict["description"],
          brainScore: moduleDict["brainScore"],
          moneyScore: moduleDict["moneyScore"],
          societyScore: moduleDict["societyScore"],
          furtherReading:
              moduleDict["furtherReading"].cast<Map<String, dynamic>>(),
        ));
      } else {
        throw Exception("Unsupported Chapter Module Type: $moduleType");
      }
    }

    // ----------------------- Load References ------------------------
    Map<String, Reference> tempReference = chapterDict['refs']
        .asMap()
        .map((id, data) => MapEntry(
            data["name"],
            Reference(
              name: data["name"],
              url: data["url"],
              refNumber: id,
            )))
        .cast<String, Reference>();

    // ------ Set Chapter, LOD, Course, and Topic in all modules ------
    for (final m in tempModuleList) {
      m.setCourse(course: course);
      m.setLevelOfDetail(levelOfDetail: levelOfDetail);
      m.setTopic(topic: topic);
      m.setChapter(chapter: this);
    }

    // -------------------------- Set things --------------------------
    moduleList = tempModuleList;
    references = tempReference;

    // ------------------------ Load Progress -------------------------
    await loadProgress();
  }

  List<ContentModuleBase> getModuleList() {
    return this.moduleList;
  }

  Map<String, Reference> getReferences() {
    return this.references;
  }

  // -------------------------- utility --------------------------
  String getChapterID() {
    return this._chapterId;
  }

  bool isCompleted() {
    return this._chapterProgress.isCompleted();
  }

  double getProgress() {
    return this._chapterProgress.getProgress();
  }

  Future loadProgress() async {
    // if not downloaded, there is nothing to load. Will default to 0
    if (isLoaded()) {
      this._chapterProgress.loadFromDisk();
      for (final m in moduleList) {
        if (m is ChallengeModuleBase) {
          m.load();
        }
      }
    }
  }

  void registerChapterOpened() {
    this._chapterProgress.registerChapterOpened();
  }

  String getChapterName() {
    return this._chapterName;
  }

  void setTopic({Topic topic}) {
    this.topic = topic;
  }

  void setCourse({Course course}) {
    this.course = course;
    this.chapterCoverImg = Image.asset(
      'assets/content/${course.getCourseID()}/$_chapterId.png',
      fit: BoxFit.contain,
    );
  }

  void setLevelOfDetail({LevelOfDetail levelOfDetail}) {
    this.levelOfDetail = levelOfDetail;
  }
}
