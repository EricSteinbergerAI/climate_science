import 'package:climate_science/frontend/common/Constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ThemeHolder {
  static const OutlineInputBorder _b = OutlineInputBorder(
    gapPadding: 0,
    borderRadius: const BorderRadius.all(const Radius.circular(15)),
    borderSide: const BorderSide(
      color: Constants.almostWhite2,
      width: 2.0,
    ),
  );

  static ThemeData themeDataClimateScienceLogin = ThemeData(
    brightness: Brightness.light,
    primaryColor: Colors.white,
    scaffoldBackgroundColor: Colors.white,
    textSelectionHandleColor: Colors.black,
    textSelectionColor: Colors.black12,
    cursorColor: Colors.white,
    unselectedWidgetColor: Constants.almostWhite2,
    toggleableActiveColor: Colors.white,
    inputDecorationTheme: InputDecorationTheme(
      contentPadding: new EdgeInsets.symmetric(vertical: 0.0, horizontal: 0.0),
      border: _b,
      enabledBorder: _b.copyWith(
        borderSide: BorderSide(
          color: Colors.white.withOpacity(0.8),
          width: 2.5,
        ),
      ),
      focusedBorder: _b,
      errorBorder: _b.copyWith(
          borderSide: const BorderSide(
        color: Constants.almostWhite2,
        width: 2.5,
      )),
      focusedErrorBorder: _b.copyWith(
          borderSide: const BorderSide(
        color: Constants.almostWhite2,
        width: 2.5,
      )),
      disabledBorder: _b,
      labelStyle: const TextStyle(
        color: Constants.almostWhite2,
        fontFamily: Constants.fontName2,
      ),
    ),
  );

  static const TextStyle errorStyle = TextStyle(
    fontFamily: Constants.fontName2,
    fontSize: 15.0,
    color: Constants.red,
    fontWeight: FontWeight.w300,
  );

  static const TextStyle formTextStyle = TextStyle(
    fontFamily: Constants.fontName2,
    fontSize: 15.0,
    color: Colors.white,
    fontWeight: FontWeight.w300,
  );

  static const TextStyle formHintStyle = TextStyle(
    fontFamily: Constants.fontName2,
    fontSize: 15.0,
    color: const Color.fromARGB(180, 255, 255, 255),
    fontWeight: FontWeight.w300,
  );

  static Widget getTextField({
    String hintText,
    String errorText,
    IconData icon,
    Function onSaved,
    bool isEmail = false,
    bool obscureText = false,
    int maxLines = 1,
    int minLines = 1,
  }) {
    return Theme(
      data: themeDataClimateScienceLogin,
      child: TextFormField(
        cursorColor: Colors.white,
        keyboardType: isEmail ? TextInputType.emailAddress : TextInputType.text,
        maxLines: maxLines,
        minLines: minLines,
        autofocus: false,
        obscureText: obscureText,
        decoration: InputDecoration(
          alignLabelWithHint: true,
          labelText: hintText,
          hintText: hintText,
          hintStyle: formHintStyle,
          errorStyle: errorStyle.copyWith(fontSize: 12),
          errorMaxLines: 100,
          helperStyle: errorStyle,
          fillColor: Colors.white,
          hoverColor: Colors.white,
          prefixIcon: icon == null ? null : Icon(
            icon,
            color: Colors.white.withOpacity(0.7),
          ),
          contentPadding: icon == null ? const EdgeInsets.fromLTRB(12, 8, 12, 8) : EdgeInsets.all(0),
        ),
        validator: (value) => value.isEmpty ? errorText : null,
        onSaved: onSaved,
        style: formTextStyle,
      ),
    );
  }
}
