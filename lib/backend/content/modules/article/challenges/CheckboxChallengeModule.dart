import 'dart:convert';
import 'dart:io' as io;

import 'ChallengeModuleBase.dart';

class CheckboxChallengeModule extends ChallengeModuleBase {
  String _question;
  List<String> _options;
  List<String> _correctAnswers;
  List<String> _userGuesses;

  CheckboxChallengeModule({
    String question,
    List<String> options,
    List<String> correctAnswers,
    String moduleId,
  }) : super(moduleId) {
    this._question = question;
    this._options = options;
    this._correctAnswers = correctAnswers;
    this._userGuesses = List<String>();
  }

  String getQuestion() {
    return this._question;
  }

  List<String> getOptions() {
    return this._options;
  }

  List<String> getCorrectAnswers() {
    return this._correctAnswers;
  }

  void registerGuess(String guess) {
    if (_userGuesses.contains(guess)) {
      _userGuesses.remove(guess);
    } else {
      _userGuesses.add(guess);
    }
    store();
  }

  @override
  void store() {
    final file = io.File(super.getFilePath());
    file.createSync(recursive: true);
    file.writeAsStringSync(json.encode({
      "base": super.getStateAsMap(),
      "guesses": _userGuesses,
    }));
  }

  List<String> getGuesses() {
    return _userGuesses;
  }

  @override
  void load() {
    final file = io.File(super.getFilePath());
    if (file.existsSync()) {
      final x = json.decode(file.readAsStringSync());
      super.loadStateFromMap(x["base"]);
      _userGuesses = x["guesses"].cast<String>();
    }
  }

  @override
  void reset() {
    _userGuesses.clear();
    super.reset();
  }
}
