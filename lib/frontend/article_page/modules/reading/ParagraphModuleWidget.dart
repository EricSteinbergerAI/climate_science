import 'package:climate_science/backend/content/modules/article/ParagraphModule.dart';
import 'package:climate_science/backend/content/modules/article/Reference.dart';
import 'package:climate_science/frontend/common/Constants.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class ParagraphModuleWidget extends StatelessWidget {
  final ParagraphModule _content;
  final bool isActive;

  ParagraphModuleWidget(this._content, {this.isActive = true});

  @override
  Widget build(BuildContext context) {
    _content.getContent();
    return Padding(
        padding: const EdgeInsets.only(bottom: 20),
        child: isActive ? _buildActive() : _buildInactive());
  }

  Widget _buildActive() {
    return RichText(
        text: TextSpan(
            children: _content.getContent().map(_createTextSpan).toList()));
  }

  Widget _buildInactive() {
    // Rough approximation so that we print a longer placeholder for longer texts.
    int numberOfLines = _content
        .getContent()
        .isEmpty ? 0 :
        (_content.getContent().map(_approximateLength).reduce((a, b) => a + b) /
                50)
            .round();
    return LayoutBuilder(
      builder: (context, constraints) {
        return Container(
          height: numberOfLines * (Constants.articleBodyTextStyle.fontSize + 4),
          child: Column(
            children: Iterable.generate(numberOfLines)
                .map((i) => Container(
                      height: Constants.articleBodyTextStyle.fontSize,
                      width: (i == numberOfLines - 1)
                          ? constraints.constrainWidth() - 60
                          : constraints.constrainWidth() - 30,
                      color: Constants.inactiveColor,
                    ))
                .toList(),
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
          ),
        );
      },
    );
  }

  int _approximateLength(RefOrTextBase e) {
    if (!(e is TextSubModule)) return 0;
    TextSubModule textSubModule = e as TextSubModule;
    return textSubModule.getText().length;
  }

  TextSpan _createTextSpan(RefOrTextBase refOrTextBase) {
    if (refOrTextBase is RefsSubModule) {
      return _buildReferences(refOrTextBase);
    }
    return TextSpan(
      text: (refOrTextBase as TextSubModule).getText(),
      style: Constants.articleBodyTextStyle,
    );
  }

  TextSpan _buildReferences(RefsSubModule refs) {
    TextStyle textStyle = Constants.articleReferenceTextStyle
        .copyWith(color: Constants.darkFontColor);
    List<TextSpan> refSpans = refs
        .getRefNames()
        .sublist(0, refs.getRefNames().length - 1)
        .map((e) => _buildReference(
              ref: _getReference(e),
              textStyle: textStyle,
              isLast: false,
            ))
        .toList();

    // build last without comma
    refSpans.add(_buildReference(
      ref: _getReference(refs.getRefNames().last),
      textStyle: textStyle,
      isLast: true,
    ));
    return TextSpan(children: <TextSpan>[
      TextSpan(text: ' (', style: textStyle),
      TextSpan(children: refSpans),
      TextSpan(text: ')', style: textStyle),
    ]);
  }

  Reference _getReference(String refName) {
    return _content.chapter.getReferences()[refName];
  }

  TextSpan _buildReference({Reference ref, bool isLast, TextStyle textStyle}) {
    return TextSpan(
      text: isLast ? '${ref.getNumber()}' : '${ref.getNumber()}, ',
      style: textStyle,
      recognizer: TapGestureRecognizer()
        ..onTap = () {
          launch(ref.getURL());
        },
    );
  }
}
