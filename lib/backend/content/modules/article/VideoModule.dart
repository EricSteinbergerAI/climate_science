import 'ContentModuleBase.dart';

class VideoModule extends ContentModuleBase {
  String _videoURL;

  VideoModule({String videoURL}) {
    this._videoURL = videoURL;
  }

  String getVideoPath() {
    return this._videoURL;
  }
}
