import 'dart:convert';
import 'dart:io' as io;

import 'package:climate_science/backend/content/CourseQuiz.dart';
import 'package:climate_science/backend/content/modules/quiz/QuizModuleBase.dart';

import '../_PrimaryProgress.dart';

class QuizProgress extends PrimaryProgress {
  CourseQuiz quiz;
  Map<String, Map<String, bool>> _state;

  QuizProgress({this.quiz});

  @override
  Future loadFromDisk() async {
    final file = io.File(getProgressDir());
    if (file.existsSync()) {
      // load
      Map<String, Map<String, bool>> map = json
          .decode(file.readAsStringSync())
          .map((id, data) => MapEntry(id, data.cast<String, bool>()))
          .cast<String, Map<String, bool>>();

      // add new keys
      for (QuizModuleBase quizModule in quiz.moduleList) {
        final String qId = quizModule.getQuestionId();
        if (map[qId] == null) {
          map[qId]['hasAttempted'] = false;
          map[qId]['isCorrect'] = null;
        }
      }
      _state = map;
      return;
    }

    // if not existing, return allFalse map
    resetProgress();
  }

  Future storeToDisk() async {
    final file = io.File(getProgressDir());
    file.createSync(recursive: true);
    file.writeAsStringSync(json.encode(_state));
  }

  @override
  String getProgressDir() {
    return '${quiz.getDir()}/progress.json';
  }

  /*
  Call this when a question is _attempted_, not just when it is correct.
   */
  void registerSolved({String questionId, bool correct}) {
    assert(_state.keys.toList().contains(questionId));
    _state[questionId]['hasAttempted'] = true;
    _state[questionId]['isCorrect'] = correct;
    storeToDisk();
  }

  double getProgress() {
    if (_state == null) {
      return 0;
    }
    double all = _state.values.length.toDouble();
    double done = this
        ._state
        .keys
        .toList()
        .where((k) => _state[k]['hasAttempted'])
        .toList()
        .length
        .toDouble();

    return done / all;
  }

  Future resetProgress() async {
    Map<String, Map<String, bool>> map = new Map();
    for (QuizModuleBase quizModule in quiz.moduleList) {
      Map<String, bool> _newStatus = new Map();
      _newStatus['hasAttempted'] = false;
      _newStatus['isCorrect'] = null;
      map[quizModule.getQuestionId()] = _newStatus;
    }
    _state = map;
  }

  void resetModuleProgress({QuizModuleBase question}) {
    final m = _state[question.getQuestionId()];
    m['hasAttempted'] = false;
    m['isCorrect'] = null;
  }

  bool isCompleted() {
    return getProgress() == 1.0;
  }
}
