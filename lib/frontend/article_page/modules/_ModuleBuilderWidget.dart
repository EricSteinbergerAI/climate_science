import 'package:climate_science/backend/content/Chapter.dart';
import 'package:climate_science/backend/content/modules/article/BarChartModule.dart';
import 'package:climate_science/backend/content/modules/article/BulletpointModule.dart';
import 'package:climate_science/backend/content/modules/article/ContentModuleBase.dart';
import 'package:climate_science/backend/content/modules/article/GraphModule.dart';
import 'package:climate_science/backend/content/modules/article/ImageModule.dart';
import 'package:climate_science/backend/content/modules/article/OpenProblemsModule.dart';
import 'package:climate_science/backend/content/modules/article/ParagraphModule.dart';
import 'package:climate_science/backend/content/modules/article/TitleModule.dart';
import 'package:climate_science/backend/content/modules/article/VideoModule.dart';
import 'package:climate_science/backend/content/modules/article/challenges/CheckboxChallengeModule.dart';
import 'package:climate_science/backend/content/modules/article/challenges/RadioBtnChallengeModule.dart';
import 'package:climate_science/frontend/article_page/modules/quiz/_CheckboxChallengeModuleWidget.dart';
import 'package:climate_science/frontend/article_page/modules/reading/BarChartModuleWidget.dart';
import 'package:climate_science/frontend/article_page/modules/reading/BulletpointsWidget.dart';
import 'package:climate_science/frontend/article_page/modules/reading/GraphModuleWidget.dart';
import 'package:climate_science/frontend/article_page/modules/reading/OpenProblemsModule.dart';
import 'package:climate_science/frontend/article_page/modules/reading/ParagraphModuleWidget.dart';
import 'package:climate_science/frontend/article_page/modules/reading/TitleModuleWidget.dart';
import 'package:flutter/material.dart';

import 'quiz/_RadioBtnChallengeModuleWidget.dart';
import 'reading/ImageModuleWidget.dart';
import 'reading/VideoModuleWidget.dart';

class ModuleBuilderWidget extends StatelessWidget {
  final ContentModuleBase module;
  final Chapter chapter;
  final Function _rebuildCallback;
  final bool isActive;

  ModuleBuilderWidget(this.chapter, this.module, this._rebuildCallback,
      {this.isActive = true});

  @override
  Widget build(BuildContext context) {
    if (module is ParagraphModule) {
      return ParagraphModuleWidget(module as ParagraphModule,
          isActive: isActive);
    }
    if (module is ImageModule) {
      return ImageModuleWidget(module as ImageModule, isActive: isActive);
    }
    if (module is VideoModule) {
      return VideoModuleWidget(module as VideoModule);
    }
    if (module is BulletpointModule) {
      return BulletpointsModuleWidget(module as BulletpointModule,
          isActive: isActive);
    }
    if (module is RadioBtnChallengeModule) {
      return RadioBtnChallengeModuleWidget(
          module as RadioBtnChallengeModule, _rebuildCallback,
          isActive: isActive);
    }
    if (module is CheckboxChallengeModule) {
      return CheckBoxChallengeModuleWidget(
          module as CheckboxChallengeModule, _rebuildCallback,
          isActive: isActive);
    }
    if (module is TitleModule) {
      return TitleModuleWidget(module as TitleModule, isActive: isActive);
    }
    if (module is BarChartModule) {
      return BarChartModuleWidget(module as BarChartModule, isActive: isActive);
    }
    if (module is GraphModule) {
      return GraphModuleWidget(module as GraphModule, isActive: isActive);
    }
    if (module is OpenProblemsModule) {
      return OpenProblemsModuleWidget(module as OpenProblemsModule, isActive: isActive);
    }

    return Text("Unknown module");
  }
}
