import 'dart:io' as io;

import 'package:climate_science/backend/authentication.dart';
import 'package:climate_science/backend/content/Chapter.dart';
import 'package:climate_science/frontend/common/Constants.dart';
import 'package:climate_science/frontend/common/dismissableAlert.dart';
import 'package:climate_science/frontend/login/theme.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class FeedbackAlertDialog extends StatefulWidget {
  Auth auth;
  Chapter chapter;

  FeedbackAlertDialog({this.auth, this.chapter});

  @override
  _FeedbackAlertDialogState createState() => _FeedbackAlertDialogState();
}

class _FeedbackAlertDialogState extends State<FeedbackAlertDialog> {
  final _formKey = GlobalKey<FormState>();
  String _feedbackText;
  final _databaseRef = Firestore.instance;

  @override
  void initState() {
    _feedbackText = "";
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return getGeneralAlert(
      context: context,
      body: Form(
        key: _formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(
              "Your feedback helps us improve Climate Science!",
              style: Constants.articleTitleTextStyle
                  .copyWith(color: Colors.white, fontSize: 20),
            ),
            const  SizedBox(
              height: 20,
            ),
            ThemeHolder.getTextField(
              isEmail: false,
              maxLines: 5,
              minLines: 3,
              hintText: "Ideas & Feedback",
              onSaved: (value) => _feedbackText = value,
            ),
          ],
        ),
      ),
      onTap: () async {
        await validateAndSubmit();
      },
      dismissText: "Send Feedback",
    );
  }

  bool validateAndSave() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  Future validateAndSubmit() async {
    try {
      if (validateAndSave()) {
        // Create our message.
        final x = _send();
        FocusScope.of(context).unfocus();
        Navigator.pop(context);
        await x;
      }
    } catch (e) {
      print('Error: $e');
      // Don't throw errors here on purpose
    }
  }

  Future _send() async {
    if (_sendFeedbackIsAllowed()) {
      String uid = (await widget.auth.getCurrentUser()).uid;
      final currentTime = DateTime.now();
      final message = {
        'uid': uid,
        'location':
            '${widget.chapter.course.getTitle()} / ${widget.chapter.levelOfDetail.asStr()} / ${widget.chapter.getChapterName()}',
        'time': currentTime,
        'message': _feedbackText
      };
      _databaseRef
          .document(
              'Feedback/${widget.chapter.course.getTitle()}-${widget.chapter.levelOfDetail.asStr()}-${widget.chapter.getChapterName()}')
          .setData({'$uid--$currentTime': message}, merge: true);
      _writeBlockFile();
    }
  }

  bool _sendFeedbackIsAllowed() {
    final blockFile = io.File('${widget.chapter.getDir()}/blockFeedback.txt');
    if ((!blockFile.existsSync())) {
      return true;
    }
    final txt = blockFile.readAsStringSync();
    final nLines = '\n'.allMatches(txt).length + 1;
    print(nLines);
    return nLines < Constants.FEEDBACK_LIMIT_PER_CHAPTER; // limit is 10
  }

  void _writeBlockFile() {
    final blockFile = io.File('${widget.chapter.getDir()}/blockFeedback.txt');
    if (blockFile.existsSync()) {
      return blockFile.writeAsStringSync(
          '${blockFile.readAsStringSync()}\n${DateTime.now()}');
    } else {
      blockFile.writeAsStringSync('${DateTime.now()}');
    }
  }
}
