import 'package:climate_science/backend/content/CourseQuiz.dart';
import 'package:climate_science/backend/content/modules/article/ContentModuleBase.dart';

class QuizModuleBase extends ContentModuleBase {
  String _questionId;
  String _feedbackMsg;
  CourseQuiz quiz;

  // backend state
  bool _checked;

  QuizModuleBase(
    String questionId,
    String feedbackMsg,
    CourseQuiz quiz,
  ) {
    this._questionId = questionId;
    this._feedbackMsg = feedbackMsg;
    this.quiz = quiz;

    this._checked = false;
  }

  String getTitle() {
    return this.course.getTitle();
  }

  String getFeedbackMsg() {
    return this._feedbackMsg;
  }

  String getQuestionId() {
    return this._questionId;
  }

  bool isChecked() {
    return this._checked;
  }

  void resetState() {
    this._checked = false;
    this.internalResetState();
    this.quiz.resetModuleProgress(question: this);
  }

  // override
  void internalResetState() {}

  // override
  bool isCorrect() {
    throw UnimplementedError();
  }

  // override
  bool attemptIsComplete() {
    throw UnimplementedError();
  }

  void registerChecked() {
    this._checked = true;
  }
}
