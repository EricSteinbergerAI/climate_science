import 'dart:io' as io;

import 'package:climate_science/backend/content/_DownloadableProgressableContent.dart';
import 'package:climate_science/frontend/common/dismissableAlert.dart';
import 'package:flutter/material.dart';

class Downloading {
  static Future<bool> doNothingIfLoadedElseUpdateAndLoad({
    BuildContext context,
    DownloadableProgressableContent downloadable,
    Function setStateCallback
  }) async {
    return await batchDownload(
      context: context,
      downloadables: <DownloadableProgressableContent>[downloadable],
        setStateCallback: setStateCallback
    );
  }

  static Future batchDownload({
    BuildContext context,
    List<DownloadableProgressableContent> downloadables,
    Function setStateCallback
  }) async {
    try {
      await Future.wait(downloadables
          .map((d) =>
          d.doNothingIfLoadedElseUpdateAndLoad(
              setStateCallback: setStateCallback))
          .toList());
      return true;
    } on io.SocketException catch (_) {
      await showDialog(
        context: context,
        builder: (BuildContext context) {
          return getDismissableAlert(
              context: context,
              formText: "We could not detect an internet connection.");
        },
      );
    } on ContentNotFoundError catch (e) {
      print("Blubby Error: $e");
      await showDialog(
        context: context,
        builder: (BuildContext context) {
          return getDismissableAlert(
              context: context,
              formText:
                  "We are currently updating this content. Please try again soon and be among the first to see the new version!");
        },
      );
    } catch (e) {
      print("Blubby Error: $e");
      await showDialog(
        context: context,
        builder: (BuildContext context) {
          return getDismissableAlert(
              context: context,
              formText:
                  "Sorry - the download failed. If you have an internet connection, this is probably a bug on our end. Please try again later!");
        },
      );
    }
    return false;
  }
}
