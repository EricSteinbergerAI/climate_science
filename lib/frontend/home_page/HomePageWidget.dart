import 'dart:async';

import 'package:climate_science/backend/authentication.dart';
import 'package:climate_science/backend/content/Topic.dart';
import 'package:climate_science/backend/content/loading/InitlialLoader.dart';
import 'package:climate_science/frontend/common/Constants.dart';
import 'package:climate_science/frontend/loading_page/LoadingPageWidget.dart';
import 'package:climate_science/frontend/social_page/SocialPageWidget.dart';
import 'package:climate_science/frontend/stats_page/StatsPageWidget.dart';
import 'package:climate_science/models/ClimateScienceUser.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import 'CoursesPageWidget.dart';

class HomePageWidget extends StatefulWidget {
  Auth auth;
  Function logoutCallback;
  ClimateScienceUser csUser;

  HomePageWidget({
    this.csUser,
    this.auth,
    this.logoutCallback,
  });

  @override
  _HomePageWidgetState createState() => _HomePageWidgetState();
}

class _HomePageWidgetState extends State<HomePageWidget> {
  List<Topic> topics;
  Future<List<Topic>> _futureTopics;
  int _selectedIndex = 1;

  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  @override
  initState() {
    super.initState();
    _futureTopics = InitialLoader.getAllTopics();
  }

  @override
  Widget build(BuildContext context) {
    if (topics == null) {
      return FutureBuilder(
        future: _futureTopics,
        builder: (context, snapshot) {
          if (snapshot.connectionState != ConnectionState.done) {
            return LoadingPageWidget("Loading courses...");
          }
          if (snapshot.hasError) {
            print('Error: ${snapshot.error}');
            return Text('Error: ${snapshot.error}');
          }
          {
            topics = snapshot.data;
            return _buildHomePage();
          }
        },
      );
    }
    return _buildHomePage();
  }

  Widget _buildHomePage() {
    return Scaffold(
      backgroundColor: Constants.bg1,
      appBar: AppBar(
        title: const Text(
          "Climate Science",
          style: Constants.appBarTextStyle,
        ),
      ),
      drawer: _buildDrawer(),
      body: Center(
        child: _buildBody(),
      ),
    );
  }

  Widget _buildBody() {
    switch (_selectedIndex) {
      case 0:
        return StatsPageWidget(topics);
      case 1:
        return CoursesPageWidget(
          topics: topics,
          auth: widget.auth,
        );
      case 2:
        return SocialPageWidget();
      default: // in case something weird happens, go to courses page
        return CoursesPageWidget(
          topics: topics,
          auth: widget.auth,
        );
    }
  }

  Widget _buildDrawer() {
    return Drawer(
      child: Column(
        children: [
          ListView(
            shrinkWrap: true,
            padding: EdgeInsets.zero,
            children: <Widget>[
              DrawerHeader(
                  child: Image.asset(
                      'assets/images/SiteIconCS_forWhiteBG_2000x400.png')),
              ListTile(
                title: const Text('About the app'),
                onTap: () {
                  launch("https://climate-science.com/");
                },
              ),
              ListTile(
                title: const Text('Contact support'),
                onTap: () {
                  launch("https://climate-science.com/contact/");
                },
              ),
              ListTile(
                title: const Text('Log Out'),
                onTap: () async {
                  await widget.logoutCallback();
                },
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 90, 15, 0),
                child: const ListTile(
                  title: const Text(
                    "Climate Science is free forever.",
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              Divider(),
            ],
          ),
          Expanded(
            child: const Padding(
              padding: const EdgeInsets.fromLTRB(0, 0, 0, 25),
              child: const Align(
                alignment: Alignment.bottomCenter,
                child: const Text('Version 1.1'),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
