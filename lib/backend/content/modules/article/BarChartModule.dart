import 'ContentModuleBase.dart';

class BarChartModule extends ContentModuleBase {
  List<DataPoint> content;
  String yAxisLabel;

  BarChartModule({
    List<Map<String, dynamic>> content,
    yAxisLabel,
  }) {
    this.content = content
        .map((e) => DataPoint(
              e["name"],
              e["value"],
            ))
        .toList()
        .cast<DataPoint>();
    this.yAxisLabel = yAxisLabel;
  }
}

class DataPoint {
  String label;
  num value;

  DataPoint(this.label, this.value);
}
