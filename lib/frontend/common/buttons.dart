import 'package:climate_science/backend/authentication.dart';
import 'package:climate_science/backend/content/Chapter.dart';
import 'package:climate_science/backend/content/CourseQuiz.dart';
import 'package:climate_science/backend/content/Topic.dart';
import 'package:climate_science/frontend/article_page/ChapterWidget.dart';
import 'package:climate_science/frontend/common/try_download.dart';
import 'package:climate_science/frontend/quiz_page/QuizWidget.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'Constants.dart';

Widget getRoundButton({
  String btnText,
  VoidCallback onPressed,
}) {
  return GestureDetector(
    onTap: onPressed,
    child: Container(
      height: 50,
      width: 270,
      decoration: const BoxDecoration(
        borderRadius: const BorderRadius.all(const Radius.circular(12)),
        color: Colors.white,
      ),
      child: Center(
        child: Text(
          btnText,
          style: const TextStyle(
            fontSize: 20.0,
            fontWeight: FontWeight.bold,
            color: Constants.buttonColor,
          ),
        ),
      ),
    ),
  );
}

Widget getButton({
  String btnText,
  VoidCallback onPressed,
  Color bgColor = Constants.buttonColor,
  Color textColor = Colors.white,
}) {
  return FlatButton(
    onPressed: onPressed,
    padding: EdgeInsets.fromLTRB(0, 12, 0, 12),
    textColor: Colors.white,
    color: bgColor,
    child: Center(
      child: Text(
        btnText,
        textAlign: TextAlign.center,
        style: TextStyle(
          color: textColor,
          fontSize: 16,
          fontFamily: Constants.fontName,
          fontWeight: FontWeight.w500,
        ),
      ),
    ),
  );
}

Widget buildQuizButton({
  BuildContext context,
  CourseQuiz quiz,
  Function afterPressed,
}) {
  return getButton(
    btnText: "START QUIZ",
    onPressed: () async {
      afterPressed();
      final success = await Downloading.doNothingIfLoadedElseUpdateAndLoad(
          context: context, downloadable: quiz,
          setStateCallback: () => {});
      if (success) {
        Navigator.of(context).pop();
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => QuizWidget(quiz),
          ),
        );
      }
    },
  );
}

Widget buildNextChapterButton({
  BuildContext context,
  Auth auth,
  Chapter currentChapter,
  Function afterPressed,
}) {
  List<Chapter> chapters = currentChapter.levelOfDetail.getChapters();
  return getButton(
    btnText: "NEXT CHAPTER",
    onPressed: () async {
      afterPressed();
      final nextChapter = chapters[chapters.indexOf(currentChapter) + 1];
      final success = await Downloading.doNothingIfLoadedElseUpdateAndLoad(
          context: context, downloadable: nextChapter,
          setStateCallback: () => {});
      if (success) {
        Navigator.pop(context);
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => ChapterWidget(
              chapter: nextChapter,
              auth: auth,
            ),
          ),
        );
      }
    },
  );
}

Widget buildGoHomeButton({
  BuildContext context,
  Topic topic,
  Function afterPressed,
}) {
  return getButton(
    btnText: "MORE COURSES",
    onPressed: () {
      afterPressed();
      Navigator.popUntil(context, ModalRoute.withName('/'));
    },
  );
}
