import 'dart:convert';
import 'dart:io' as io;

import 'ChallengeModuleBase.dart';

class RadioBtnChallengeModule extends ChallengeModuleBase {
  String _question;
  List<String> _options;
  String _correctAnswer;
  String _userGuess;

  RadioBtnChallengeModule({
    String question,
    List<String> options,
    String correctAnswer,
    String moduleId,
  }) : super(moduleId) {
    this._question = question;
    this._options = options;
    this._correctAnswer = correctAnswer;
  }

  String getQuestion() {
    return this._question;
  }

  List<String> getOptions() {
    return this._options;
  }

  String getCorrectAnswer() {
    return this._correctAnswer;
  }

  void registerGuess(String guess) {
    this._userGuess = guess;
    this.registerAttempted();
    store();
  }

  String getGuess() {
    return _userGuess;
  }

  @override
  void store() {
    final file = io.File(super.getFilePath());
    file.createSync(recursive: true);
    file.writeAsStringSync(json.encode({
      "base": super.getStateAsMap(),
      "guess": _userGuess,
    }));
  }

  @override
  void load() {
    final file = io.File(super.getFilePath());
    if (file.existsSync()) {
      final x = json.decode(file.readAsStringSync());
      super.loadStateFromMap(x["base"]);
      _userGuess = x["guess"];
    }
  }

  @override
  void reset() {
    _userGuess = null;
    super.reset();
  }
}
