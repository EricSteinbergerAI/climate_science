import 'dart:async';

import 'package:climate_science/backend/authentication.dart';
import 'package:climate_science/csUtil.dart' as csUtil;
import 'package:climate_science/frontend/common/Constants.dart';
import 'package:climate_science/frontend/common/buttons.dart';
import 'package:climate_science/frontend/common/dismissableAlert.dart';
import 'package:climate_science/frontend/loading_page/LoadingPageWidget.dart';
import 'package:climate_science/frontend/login/PasswordResetAlert.dart';
import 'package:climate_science/frontend/login/theme.dart';
import 'package:climate_science/models/ClimateScienceUser.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

enum PageStatus {
  LOGIN,
  SIGNUP,
}

class LoginSignupPage extends StatefulWidget {
  Auth auth;
  final AsyncCallback loginCallback;
  final AsyncCallback minorSignUpCallback;
  PageStatus status;

  LoginSignupPage({
    this.auth,
    this.loginCallback,
    this.minorSignUpCallback,
    this.status,
  });

  @override
  _LoginSignupPageState createState() => _LoginSignupPageState(status);
}

class _LoginSignupPageState extends State<LoginSignupPage>
    with SingleTickerProviderStateMixin {
  final _formKey = GlobalKey<FormState>();

  PageStatus status;

  String _email;
  String _password;
  String _passwordConfirm;
  String _errorMessage;
  bool _agreedToTOS = true; // does so by signing up

  bool _isLoading;

  bool _answeredIsMinor = false;
  bool _isMinor = true;

  _LoginSignupPageState(this.status);

  @override
  void initState() {
    _errorMessage = "";
    _isLoading = false;
    super.initState();
  }

  // ************************* Screens & Widgets *************************
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          _getMainWiget(),
          _isLoading
              ? LoadingPageWidget("Authenticating...")
              : Container(
                  height: 0.0,
                  width: 0.0,
                ),
        ],
      ),
    );
  }

  Widget _getMainWiget() {
    return LayoutBuilder(
      builder: (context, constraints) {
        return SingleChildScrollView(
          child: ConstrainedBox(
            constraints: BoxConstraints(
              minWidth: constraints.maxWidth,
              minHeight: constraints.maxHeight,
            ),
            child: Container(
              decoration: const BoxDecoration(
                gradient: const LinearGradient(
                  colors: [Color(0xFF8185E2), Constants.buttonColor],
                  begin: const Alignment(-1.0, -1.0),
                  end: const Alignment(1.0, 1.0),
                ),
              ),
              child: Padding(
                padding: const EdgeInsets.fromLTRB(
                  35,
                  55,
                  35,
                  0,
                ),
                child: ListView(
                  shrinkWrap: true,
                  children: <Widget>[
                    Image.asset(
                      'assets/images/SiteIconCS_forColorBG_2000x400.png',
                    ),
                    const SizedBox(height: 25),
                    Form(
                      key: _formKey,
                      child: status == PageStatus.LOGIN
                          ? _getLoginForm()
                          : _getSignUpForm(),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  Widget _getLoginForm() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        ThemeHolder.getTextField(
          isEmail: true,
          hintText: "Email",
          errorText: "Please enter your email",
          icon: Icons.person,
          onSaved: (value) => _email = value,
        ),
        const SizedBox(height: 15),
        ThemeHolder.getTextField(
          hintText: 'Password',
          errorText: 'Please enter your password',
          obscureText: true,
          icon: Icons.lock_outline,
          onSaved: (value) => _password = value,
        ),
        InkWell(
          onTap: () async {
            await showDialog(
              context: context,
              builder: (BuildContext context) {
                return PasswordResetAlertDialog(auth: widget.auth);
              },
            );
            await showDialog(
              context: context,
              builder: (BuildContext context) {
                return getDismissableAlert(
                    context: context,
                    formText:
                        "Reset your password by clicking the link we sent via email.");
              },
            );
          },
          child: Container(
            width: double.infinity, // needed to get the thing to the right
            padding: const EdgeInsets.only(top: 8),
            child: const Text(
              "Forgot password?",
              textAlign: TextAlign.right,
              style: ThemeHolder.formTextStyle,
            ),
          ),
        ),
        _showErrorMessage(),
        const SizedBox(height: 35),
        getRoundButton(
          btnText: 'Login',
          onPressed: () async {
            await validateAndSubmit();
          },
        ),
        const SizedBox(height: 20),
        _getSecondaryButton(),
        const SizedBox(height: 40),
      ],
    );
  }

  Widget _getSignUpForm() {
    return Column(
      children: <Widget>[
        Column(
          children: <Widget>[
            _getAgeField(),
            const SizedBox(height: 25),
          ],
        ),
        Visibility(
          visible: (_answeredIsMinor),
          child: Column(
            children: <Widget>[
              ThemeHolder.getTextField(
                isEmail: true,
                hintText: _isMinor ? "Parent's email address" : "Email",
                errorText: _isMinor
                    ? "Please enter an email of your parents"
                    : "Please enter an email address.",
                icon: Icons.person,
                onSaved: (value) => _email = value,
              ),
              const SizedBox(height: 15),
              ThemeHolder.getTextField(
                hintText: 'Password',
                errorText: 'Please enter a password',
                obscureText: true,
                icon: Icons.lock_outline,
                onSaved: (value) => _password = value,
              ),
              const SizedBox(height: 15),
              Column(
                children: <Widget>[
                  ThemeHolder.getTextField(
                    hintText: 'Confirm Password',
                    errorText: 'Please confirm your password',
                    obscureText: true,
                    icon: Icons.lock_outline,
                    onSaved: (value) => _passwordConfirm = value,
                  ),
                  const SizedBox(height: 10),
                ],
              ),
              _showErrorMessage(),
              const SizedBox(height: 35),
              getRoundButton(
                btnText: 'Create Account',
                onPressed: () async {
                  await validateAndSubmit();
                },
              ),
              const SizedBox(height: 20),
              _getSecondaryButton(),
              const SizedBox(height: 40),
              _getTOSField(),
              const SizedBox(height: 10),
            ],
          ),
        ),
      ],
    );
  }

  Widget _getAgeField() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        RaisedButton(
          child: const Text("I'm over 16"),
          onPressed: () {
            setState(() {
              this._isMinor = false;
              this._answeredIsMinor = true;
            });
          },
          textColor: Constants.buttonColor,
          color: (_answeredIsMinor && this._isMinor)
              ? Constants.almostWhite2
              : Constants.niceColor4,
          shape: const RoundedRectangleBorder(
              borderRadius: const BorderRadius.only(
                  topLeft: const Radius.circular(10),
                  bottomLeft: const Radius.circular(10))),
        ),
        RaisedButton(
          child: const Text("I'm under 16"),
          onPressed: () {
            setState(() {
              this._isMinor = true;
              this._answeredIsMinor = true;
            });
          },
          textColor: Constants.buttonColor,
          color: this._isMinor ? Constants.niceColor4 : Constants.almostWhite2,
          shape: const RoundedRectangleBorder(
              borderRadius: const BorderRadius.only(
                  topRight: const Radius.circular(10),
                  bottomRight: const Radius.circular(10))),
        )
      ],
    );
  }

  Widget _getTOSField() {
    TextStyle s1 = ThemeHolder.formTextStyle.copyWith(
        color: const Color.fromARGB(255, 211, 211, 211), fontSize: 13);
    TextStyle s2 =
        ThemeHolder.formTextStyle.copyWith(fontSize: 13, color: Colors.white54);
    return RichText(
      textAlign: TextAlign.center,
      text: TextSpan(children: <TextSpan>[
        TextSpan(
          text: 'By tapping Create Account, I agree to Climate Science\'s ',
          style: s1,
        ),
        TextSpan(
          text: 'Terms',
          recognizer: TapGestureRecognizer()
            ..onTap = () {
              launch('https://climate-science.com/terms-of-use/');
            },
          style: s2,
        ),
        TextSpan(
          text: ' and ',
          style: s1,
        ),
        TextSpan(
          text: 'Privacy Policy',
          recognizer: TapGestureRecognizer()
            ..onTap = () {
              launch('https://climate-science.com/privacy-policy/');
            },
          style: s2,
        ),
        TextSpan(
          text: '.',
          style: s1,
        ),
      ]),
    );
  }

  // ************************* form verification *************************
  void _setAgreedToTOS(bool newValue) {
    setState(() {
      _agreedToTOS = newValue;
    });
  }

  bool validateAndSave() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    _isLoading = false;
    return false;
  }

  void throwCostumInsideFormException({String text}) {
    _isLoading = false;
    throw Exception(text);
  }

  Future<void> validateAndSubmit() async {
    setState(() {
      csUtil.dismissKeyboard(context: context);
      _errorMessage = "";
      _isLoading = true;
    });
    if (validateAndSave()) {
      try {
        final result = _submit();
        await result.timeout(Duration(seconds: 10));
      } on TimeoutException {
        setState(() {
          _isLoading = false;
          _errorMessage = "Request timed out. Please try again.";
        });
      } catch (e) {
        print('Error: $e');
        setState(() {
          try {
            // might fail if error has no code, thus try.
            switch (e.code) {
              case "ERROR_INVALID_EMAIL":
                _errorMessage = "Invalid email address.";
                break;
              case "ERROR_WRONG_PASSWORD":
              case "ERROR_USER_NOT_FOUND":
                _errorMessage = "Email or password is incorrect.";
                break;
              case "ERROR_USER_DISABLED":
                _errorMessage = "Your account has been disabled.";
                break;
              case "ERROR_TOO_MANY_REQUESTS":
              case "ERROR_OPERATION_NOT_ALLOWED":
                _errorMessage =
                    "Our login servers are currently out of service.";
                break;
              case "ERROR_WEAK_PASSWORD":
                _errorMessage =
                    "Weak password. Please use at least 6 characters.";
                break;
              case "ERROR_EMAIL_ALREADY_IN_USE":
                _errorMessage = "Email is already taken.";
                break;

              default:
                _errorMessage = "Unexpected error. Please check your connection or try again later.";
                break;
            }
          } catch (e2) {
            _errorMessage = "Unexpected error. Please check your connection or try again later.";
          }
          _isLoading = false;
          _formKey.currentState.reset();
        });
      }
    }
  }

  Future<void> _submit() async {
    ClimateScienceUser csUser;
    if (status == PageStatus.SIGNUP) {
      if (_passwordConfirm != _password) {
        throwCostumInsideFormException(text: "Passwords do not match!");
      }
      if (!_agreedToTOS) {
        throwCostumInsideFormException(
            text: "Please review and accept our Privacy Policy and TOS");
      }
    }

    if (status == PageStatus.LOGIN) {
      csUser = await widget.auth.signIn(_email, _password);
    } else {
      csUser = await widget.auth.signUp(
        email: _email,
        password: _password,
        isMinor: _isMinor,
      );
    }

    setState(() {
      _isLoading = false;
    });

    if (csUser != null && csUser.uid != null && csUser.uid.length > 0) {
      if (status == PageStatus.SIGNUP) {
        widget.auth.sendEmailVerification();
      }

      if (status == PageStatus.SIGNUP && _isMinor) {
        await widget.minorSignUpCallback();
      } else {
        await widget.loginCallback();
      }
    }
  }

  void resetForm() {
    _formKey.currentState.reset();
    _errorMessage = "";
  }

  Widget _showErrorMessage() {
    if (_errorMessage != null && _errorMessage.length > 0) {
      return Padding(
        padding: const EdgeInsets.fromLTRB(0, 25, 0, 0),
        child: Center(
          child: Text(
            _errorMessage,
            style: ThemeHolder.errorStyle,
            textAlign: TextAlign.center,
            maxLines: 15,
          ),
        ),
      );
    } else {
      return Container(height: 0.0);
    }
  }

  Widget _getSecondaryButton() {
    return GestureDetector(
      onTap: () => setState(() {
        csUtil.dismissKeyboard(context: context);
        resetForm();
        status =
            status == PageStatus.SIGNUP ? PageStatus.LOGIN : PageStatus.SIGNUP;
      }),
      child: Center(
        child: status == PageStatus.SIGNUP
            ? const Text(
                'Have an account? Sign in',
                style: ThemeHolder.formTextStyle,
                textAlign: TextAlign.center,
              )
            : const Text(
                'Don\'t have an account? Sign up',
                style: ThemeHolder.formTextStyle,
                textAlign: TextAlign.center,
              ),
      ),
    );
  }
}
