import 'dart:math' as math;

import 'package:flutter/material.dart';

import 'Constants.dart';

class GradientProgressWidget extends StatelessWidget {
  final double value;
  final double height;
  final Color backgroundColor;
  final bool isRounded;
  final bool blackLineTop;
  final bool constantZero;

  final Radius circularRadius;

  GradientProgressWidget({
    this.value,
    this.isRounded = false,
    this.backgroundColor,
    this.height,
    this.blackLineTop = false,
    this.circularRadius = const Radius.circular(12),
    this.constantZero = false,
  });

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
      return Column(children: <Widget>[
        Container(
          height: blackLineTop ? 1.5 : 0,
          width: constraints.maxWidth,
          decoration: const BoxDecoration(color: Colors.black87),
        ),
        Container(
            height: this.height,
            alignment: Alignment.centerRight,
            width: constraints.maxWidth,
            decoration: BoxDecoration(
                borderRadius: _mainSectionRadius(),
                gradient: const LinearGradient(colors: [
//                  Color.fromARGB(255, 255, 40, 153),
//                  Color.fromARGB(255, 255, 167, 38),
                  Constants.red,
                  Constants.gradientR,
                ])),
            child: Container(
              height: this.height,
              width: constantZero
                  ? constraints.maxWidth
                  : (value.isNaN
                      ? 0.92
                      : math.min((1 - value), 0.92) * constraints.maxWidth),
              decoration: BoxDecoration(
                borderRadius: constantZero
                    ? _coveringSectionRadiusFull()
                    : _coveringSectionRadius(),
                color: backgroundColor == null ? Colors.white : backgroundColor,
              ),
            ))
      ]);
    });
  }

  BorderRadius _mainSectionRadius() {
    if (isRounded) return BorderRadius.all(circularRadius);
    return BorderRadius.only(
        bottomRight: circularRadius, bottomLeft: circularRadius);
  }

  BorderRadius _coveringSectionRadius() {
    if (isRounded)
      return BorderRadius.only(
        bottomRight: circularRadius,
        topRight: circularRadius,
      );
    return BorderRadius.only(bottomRight: circularRadius);
  }

  BorderRadius _coveringSectionRadiusFull() {
    if (isRounded) return BorderRadius.all(circularRadius);
    return BorderRadius.only(
        bottomRight: circularRadius, bottomLeft: circularRadius);
  }
}
