import 'package:climate_science/backend/content/modules/article/VideoModule.dart';
import 'package:flutter/material.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

class VideoModuleWidget extends StatefulWidget {
  final VideoModule content;

  VideoModuleWidget(this.content);

  @override
  State<StatefulWidget> createState() {
    return VideoModuleWidgetState(content);
  }
}

class VideoModuleWidgetState extends State<VideoModuleWidget> {
  VideoModule content;

  VideoModuleWidgetState(this.content);

  YoutubePlayerController _controller = YoutubePlayerController();
  double _volume = 100;

  String _videoId;

  @override
  void deactivate() {
    // This pauses video while navigating to next page.
    _controller.pause();
    super.deactivate();
  }

  @override
  Widget build(BuildContext context) {
    _videoId = YoutubePlayer.convertUrlToId(content.getVideoPath());
    throw UnimplementedError(); // TODO The old versio of this package caused crashes. Updated version has changed interface :C
//    return Padding(
//      padding: EdgeInsets.fromLTRB(0, 15, 0, 15),
//      child: Column(
//        children: <Widget>[
//          YoutubePlayer(
//            context: context,
//            videoId: _videoId,
//            flags: YoutubePlayerFlags(
//              mute: false,
//              autoPlay: false,
//              forceHideAnnotation: true,
//              showVideoProgressIndicator: true,
//              disableDragSeek: false,
//            ),
//            videoProgressIndicatorColor: const Color(0xFFFF0000),
//            progressColors: ProgressColors(
//              playedColor: const Color(0xFFFF0000),
//              handleColor: const Color(0xFFFF4433),
//            ),
//            onPlayerInitialized: (controller) {
//              _controller = controller;
//            },
//          ),
//          SizedBox(
//            height: 10.0,
//          ),
//          Padding(
//            padding: EdgeInsets.all(8.0),
//            child: Column(
//              crossAxisAlignment: CrossAxisAlignment.stretch,
//              children: <Widget>[
//                Row(
//                  children: <Widget>[
//                    const Text(
//                      "Volume",
//                      style: TextStyle(fontWeight: FontWeight.w300),
//                    ),
//                    Expanded(
//                      child: Slider(
//                        inactiveColor: Colors.transparent,
//                        value: _volume,
//                        min: 0.0,
//                        max: 100.0,
//                        divisions: 10,
//                        label: '${(_volume).round()}',
//                        onChanged: (value) {
//                          setState(() {
//                            _volume = value;
//                          });
//                          _controller.setVolume(_volume.round());
//                        },
//                      ),
//                    ),
//                  ],
//                ),
//              ],
//            ),
//          ),
//        ],
//      ),
//    );
  }
}
