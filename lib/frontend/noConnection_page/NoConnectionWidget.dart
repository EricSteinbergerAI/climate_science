import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class NoConnectionWidget extends StatefulWidget {
  NoConnectionWidget({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _NoConnectionWidgetState createState() => _NoConnectionWidgetState();
}

class _NoConnectionWidgetState extends State<NoConnectionWidget> {
  @override
  Widget build(BuildContext context) {
    return const Text(
      'No Connection found :/  Please try again with internet turned on!',
    );
  }
}
