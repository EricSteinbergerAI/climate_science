import 'package:climate_science/backend/content/Chapter.dart';
import 'package:climate_science/backend/content/Course.dart';
import 'package:climate_science/backend/content/LevelOfDetail.dart';
import 'package:climate_science/backend/content/Topic.dart';

class ContentModuleBase {
  Topic topic;
  Course course;
  LevelOfDetail levelOfDetail;
  Chapter chapter;

  ContentModuleBase();

  void setTopic({Topic topic}) {
    this.topic = topic;
  }

  void setCourse({Course course}) {
    this.course = course;
  }

  void setLevelOfDetail({LevelOfDetail levelOfDetail}) {
    this.levelOfDetail = levelOfDetail;
  }

  void setChapter({Chapter chapter}) {
    this.chapter = chapter;
  }
}
