import 'package:avatar_glow/avatar_glow.dart';
import 'package:climate_science/backend/authentication.dart';
import 'package:climate_science/frontend/common/Constants.dart';
import 'package:climate_science/frontend/common/buttons.dart';
import 'package:climate_science/frontend/login/LoginSignupPage.dart';
import 'package:climate_science/frontend/login/theme.dart';
import 'package:delayed_display/delayed_display.dart';
import 'package:flutter/material.dart';

class WelcomePage extends StatefulWidget {
  Auth auth;
  final VoidCallback loginCallback;
  final VoidCallback minorSignUpCallback;

  WelcomePage({
    this.auth,
    this.loginCallback,
    this.minorSignUpCallback,
  });

  @override
  _WelcomePageState createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage>
    with SingleTickerProviderStateMixin {
  @override
  void initState() {
    super.initState();
  }

  // ************************* Screens & Widgets *************************
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: LayoutBuilder(
        builder: (context, constraints) {
          return SingleChildScrollView(
            child: ConstrainedBox(
              constraints: BoxConstraints(
                  minWidth: constraints.maxWidth,
                  minHeight: constraints.maxHeight),
              child: Container(
                width: double.infinity,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [Color(0xFF8185E2), Constants.buttonColor],
                    begin: Alignment(-1.0, -1.0),
                    end: Alignment(1.0, 1.0),
                  ),
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    const SizedBox(
                      height: 70,
                    ),
                    AvatarGlow(
                      endRadius: 114,
                      duration: Duration(seconds: 2),
                      glowColor: Colors.white24,
                      repeat: true,
                      repeatPauseDuration: Duration(seconds: 2),
                      startDelay: Duration(seconds: 1),
                      child: Material(
                        elevation: 8.0,
                        shape: CircleBorder(),
                        child: CircleAvatar(
                          backgroundColor: Colors.grey[100],
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(1000),
                            child: Image.asset(
                              'assets/images/LogoWithWhiteBG_2048.png',
                            ),
                          ),
                          radius: 65.0,
                        ),
                      ),
                    ),
                    DelayedDisplay(
                      child: Text(
                        "Climate Science",
                        style: TextStyle(
                            fontWeight: FontWeight.w500,
                            fontFamily: Constants.fontName2,
                            fontSize: 35.0,
                            color: Colors.white),
                        textAlign: TextAlign.center,
                      ),
                      delay: Duration(milliseconds: 600),
                    ),
                    DelayedDisplay(
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(10, 10, 10, 25),
                        child: Text(
                          "Learn, solve, act.",
                          style: Constants.articleBodyTextStyle.copyWith(
                              color: Constants.almostWhite,
                              fontSize: 17,
                              fontWeight: FontWeight.w500),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      delay:const Duration(milliseconds: 1000),
                    ),
                    const  SizedBox(
                      height: 65.0,
                    ),
                    DelayedDisplay(
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(35, 0, 35, 0),
                        child: getRoundButton(
                          onPressed: () => setState(
                            () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => LoginSignupPage(
                                    auth: widget.auth,
                                    loginCallback: widget.loginCallback,
                                    minorSignUpCallback:
                                        widget.minorSignUpCallback,
                                    status: PageStatus.SIGNUP,
                                  ),
                                ),
                              );
                            },
                          ),
                          btnText: 'Sign Up',
                        ),
                      ),
                      delay: const Duration(milliseconds: 1500),
                    ),
                    const SizedBox(
                      height: 20.0,
                    ),
                    DelayedDisplay(
                      child: GestureDetector(
                        onTap: () => setState(
                          () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => LoginSignupPage(
                                  auth: widget.auth,
                                  loginCallback: widget.loginCallback,
                                  status: PageStatus.LOGIN,
                                ),
                              ),
                            );
                          },
                        ),
                        child: Center(
                          child: Text(
                            'Have an account? Sign in',
                            style: ThemeHolder.formTextStyle,
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                      delay: Duration(milliseconds: 1750),
                    ),
                    const  SizedBox(
                      height: 50.0,
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
