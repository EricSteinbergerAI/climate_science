import 'dart:async';
import 'dart:io' as io;

import 'package:flutter/cupertino.dart';

Future<bool> haveConnection() async {
  try {
    final result = await io.InternetAddress.lookup('example.com');
    if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
      return true;
    }
  } on io.SocketException catch (_) {
    return false;
  }
  return false;
}

void dismissKeyboard({context}) {
  FocusScopeNode currentFocus = FocusScope.of(context);
  if (!currentFocus.hasPrimaryFocus) {
    currentFocus.unfocus();
  }
  // SystemChannels.textInput.invokeMethod('TextInput.hide');
}
