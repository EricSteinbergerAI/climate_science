import 'package:avatar_glow/avatar_glow.dart';
import 'package:climate_science/frontend/common/Constants.dart';
import 'package:climate_science/frontend/common/progressbars.dart';
import 'package:flutter/material.dart';

class LoadingPageWidget extends StatelessWidget {
  final String _text;

  LoadingPageWidget(this._text);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            AvatarGlow(
              endRadius: 114,
              duration: Duration(seconds: 2),
              glowColor: Constants.normalText,
              repeat: true,
              repeatPauseDuration: Duration(seconds: 1),
              startDelay: Duration(milliseconds: 400),
              child: Material(
                elevation: 8.0,
                shape: CircleBorder(),
                child: CircleAvatar(
                  backgroundColor: Colors.grey[100],
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(1000),
                    child: Image.asset(
                      'assets/images/LogoWithWhiteBG_2048.png',
                    ),
                  ),
                  radius: 65.0,
                ),
              ),
            ),
            SizedBox(
              height: 30,
            ),
            getCircularProgress(),
            SizedBox(
              height: 20,
            ),
            Text(
              _text,
              textAlign: TextAlign.center,
              style: Constants.articleBodyTextStyle,
            ),
          ],
        ),
      ),
    );
  }
}
