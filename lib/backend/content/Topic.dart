import 'package:climate_science/backend/content/progress/TopicProgress.dart';

import 'Course.dart';

class Topic {
  String _name;
  String _shortName;
  List<Course> _courses;
  TopicProgress _topicProgress;

  Topic({
    String name,
    String shortName,
    List<Course> courses,
  }) {
    this._name = name;
    this._shortName = shortName;

    this._courses = courses;
    this._topicProgress = TopicProgress(topic: this);
  }

  Future init() async {
    for (var c in _courses) {
      c.setTopic(topic: this);
    }
    await Future.wait(this._courses.map((c) => c.init()));
  }

  String getName() {
    return this._name;
  }

  String getShortName() {
    return this._shortName;
  }

  List<Course> getCourses() {
    return this._courses;
  }

  double getProgress() {
    return this._topicProgress.getProgress();
  }

  bool isCompleted() {
    return this._topicProgress.isCompleted();
  }
}
