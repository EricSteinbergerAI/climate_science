import 'ContentModuleBase.dart';

class BulletpointModule extends ContentModuleBase {
  List<String> _points;

  BulletpointModule({
    List<String> points,
  }) {
    this._points = points;
  }

  List<String> getPoints() {
    return this._points;
  }
}
