import 'dart:convert';

import 'package:climate_science/backend/content/Chapter.dart';
import 'package:climate_science/backend/content/Course.dart';
import 'package:climate_science/backend/content/LevelOfDetail.dart';
import 'package:climate_science/backend/content/Topic.dart';
import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';

import '../CourseQuiz.dart';

class InitialLoader {
  // Note: Use this method to get a list of all courses.
  static Future<List<Topic>> getAllTopics() async {
    final List<Map<String, dynamic>> topicsMapOrdered = await json
        .decode(await rootBundle.loadString("assets/content/content.json"))[
            "ordered"]
        .cast<Map<String, dynamic>>();
    final String appSupportDir = (await getApplicationSupportDirectory()).path;
    final m = topicsMapOrdered
        .map(
          (topicDict) => Topic(
            name: topicDict["name"],
            shortName: topicDict["shortName"],
            courses: topicDict["courses"]
                .map(
                  (courseDict) => Course(
                    courseId: courseDict["courseId"],
                    name: courseDict["name"],
                    summary: courseDict["summary"],
                    isPublished: courseDict["isPublished"] == "true",
                    levelsOfDetail: courseDict["chapters"]
                        .map(
                          (lodAsStr, chapterListLOD) => MapEntry(
                            lodAsStr,
                            LevelOfDetail(
                              lodAsStr: lodAsStr,
                              courseQuiz: CourseQuiz(
                                appSupportDir: appSupportDir,
                              ),
                              chapterList: chapterListLOD
                                  .map((chapterDict) => Chapter(
                                        chapterId: chapterDict["chapterId"],
                                        chapterName: chapterDict["name"],
                                        appSupportDir: appSupportDir,
                                      ))
                                  .toList()
                                  .cast<Chapter>(),
                            ),
                          ),
                        )
                        .cast<String, LevelOfDetail>(),
                  ),
                )
                .toList()
                .cast<Course>(),
          ),
        )
        .toList()
        .cast<Topic>();

    await Future.wait(m.map((topic) => topic.init()));
    return m;
  }
}
