import 'dart:io';

import 'package:climate_science/backend/content/modules/article/ImageModule.dart';
import 'package:climate_science/frontend/common/Constants.dart';
import 'package:flutter/material.dart';

class ImageModuleWidget extends StatelessWidget {
  final ImageModule _content;
  final bool isActive;

  ImageModuleWidget(this._content, {this.isActive = true});

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
        child: isActive
            ? Image.file(
                File(
                  _content.getImagePath(),
                ),
              )
            : Container(
                height: 200,
                color: Constants.inactiveColor,
              ));
  }
}
