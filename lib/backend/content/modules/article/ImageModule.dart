import 'ContentModuleBase.dart';

class ImageModule extends ContentModuleBase {
  String _imagePath;

  ImageModule({String imgPath}) {
    this._imagePath = imgPath;
  }

  String getImagePath() {
    return this._imagePath;
  }
}
