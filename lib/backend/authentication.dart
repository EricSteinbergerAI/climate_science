import 'dart:async';

import 'package:climate_science/models/ClimateScienceUser.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class Auth {
  final FirebaseAuth mAuth = FirebaseAuth.instance;
  final databaseRef = Firestore.instance;

  Future<ClimateScienceUser> signIn(String email, String password) async {
    AuthResult result = await mAuth.signInWithEmailAndPassword(
        email: email, password: password);
    return await _makeCSUser(firebaseUser: result.user);
  }

  Future<ClimateScienceUser> signUp({
    String email,
    String password,
    bool isMinor,
  }) async {
    AuthResult result = await mAuth.createUserWithEmailAndPassword(
        email: email, password: password);
    await databaseRef.document("Users/${result.user.uid}").setData({
      "isMinor": isMinor,
    });
    return await _makeCSUser(firebaseUser: result.user, isMinor:isMinor);
  }

  Future<ClimateScienceUser> getCurrentUser() async {
    return await _makeCSUser(firebaseUser: await mAuth.currentUser());
  }

  Future<void> signOut() async {
    return await mAuth.signOut();
  }

  Future<void> sendEmailVerification() async {
    FirebaseUser user = await mAuth.currentUser();
    await user.sendEmailVerification();
  }

  Future<void> resetPassword(String email) async {
    await mAuth.sendPasswordResetEmail(email: email);
  }

  Future<bool> isEmailVerified() async {
    FirebaseUser user = await mAuth.currentUser();
    return user.isEmailVerified;
  }

  Future<ClimateScienceUser> _makeCSUser({FirebaseUser firebaseUser, bool isMinor}) async {
    if (firebaseUser == null) return null;
    assert(firebaseUser.uid != null);
    final doc = await databaseRef.document("Users/${firebaseUser.uid}").get();
    if (doc.exists) {
      return ClimateScienceUser(
              uid: firebaseUser.uid,
              email: firebaseUser.email,
              isMinor: doc.data["isMinor"],
              emailVerified: firebaseUser.isEmailVerified,
            );
    } else {
      print(
          "ClimSci PROBLEM: Users table does not contain isMinor for given user");
      //TODO
    }
    return null;
  }
}
