import 'package:climate_science/backend/authentication.dart';
import 'package:climate_science/backend/content/Course.dart';
import 'package:climate_science/backend/content/Topic.dart';
import 'package:climate_science/frontend/common/Constants.dart';
import 'package:climate_science/frontend/common/GradientProgressWidget.dart';
import 'package:climate_science/frontend/course_page/CoursePageWidget.dart';
import 'package:flutter/material.dart';

class CoursesPageWidget extends StatefulWidget {
  final List<Topic> topics;
  Auth auth;

  CoursesPageWidget({this.topics, this.auth});

  @override
  _CoursesPageWidgetState createState() =>
      _CoursesPageWidgetState();
}

class _CoursesPageWidgetState extends State<CoursesPageWidget> {
  static const double radiusDouble = 10;
  static const Radius radius = Radius.circular(radiusDouble);
  static const BorderRadius borderRadius = BorderRadius.all(radius);


  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.fromLTRB(0.0, 18, 0.0, 4),
              child: Center(
                child: const Text(
                  'Courses',
                  style: Constants.pageTitleTextStyle,
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: widget.topics.map(
                (topic) {
                  return _buildTopic(topic);
                },
              ).toList(),
            ),
            _buildClimateScienceGoodbye(),
          ],
        ),
      ),
    );
  }

  Widget _buildTopic(Topic topic) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
              padding: const EdgeInsets.fromLTRB(25, 21, 10, 3),
              child: Text(
                topic.getName(),
                style: Constants.coursesPageTopicTextStyle,
              )),
          Padding(
            padding: const EdgeInsets.fromLTRB(4, 0, 4, 0),
            child: LayoutBuilder(
                builder: (BuildContext context, BoxConstraints constraints) {
              return Column(
                children: <Widget>[
                  _buildGrid(
                    courses: topic.getCourses(),
                    maxCardWidth: constraints.maxWidth / 2,
                    bgColor: Constants.courseCardColor,
                  ),
                ],
              );
            }),
          )
        ],
      ),
    );
  }

  Widget _buildGrid({
    List<Course> courses,
    double maxCardWidth,
    Color bgColor,
  }) {
    const double padd = 4;
    List<Widget> rows = List<Widget>();
    for (int row = 0; row * 2 < courses.length; row++) {
      int i = row * 2;
      if (i < courses.length - 1)
        rows.add(Row(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.fromLTRB(padd * 2, 0, padd, padd * 2),
              child: _buildCourseCard(
                course: courses[i],
                width: maxCardWidth - padd * 3,
                color: bgColor,
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(padd, 0, padd * 2, padd * 2),
              child: _buildCourseCard(
                  course: courses[i + 1],
                  width: maxCardWidth - padd * 3,
                  color: bgColor),
            ),
          ],
        ));
      else
        rows.add(
          Padding(
            padding: const EdgeInsets.fromLTRB(padd * 2, 0, padd, padd * 2),
            child: _buildCourseCard(
              course: courses[i],
              width: maxCardWidth - padd * 3,
              color: bgColor,
            ),
          ),
        );
    }
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: rows,
    );
  }

  Widget _buildClimateScienceGoodbye() {
    // TODO draw a happy Earthly image with a finish line in his hands.
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 15, 0, 50),
      child: const Divider(),
    );
  }

  Widget _buildCourseCard({Course course, double width, Color color}) {
    return Container(
      decoration: const BoxDecoration(
        borderRadius: borderRadius,
      ),
      width: width,
      height: width,
      child: Card(
        shape: const RoundedRectangleBorder(
          borderRadius: borderRadius,
        ),
        elevation: 2,
        color: color,
        child: ClipRRect(
          borderRadius: const BorderRadius.only(
            topLeft: radius,
            topRight: radius,
          ),
          child: InkWell(
            splashColor: Colors.blue.withAlpha(30),
            onTap: course.isPublished()
                ? () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => CoursePageWidget(
                          course: course,
                          auth: widget.auth,
                        ),
                      ),
                    );
                  }
                : () {},
            child: Stack(children: <Widget>[
              Image.asset(
                  'assets/content/${course.getCourseID()}/${course.getCourseID()}.png'),
//              course.isPublished()
//                  ? Column(
//                      crossAxisAlignment: CrossAxisAlignment.center,
//                      mainAxisSize: MainAxisSize.max,
//                      mainAxisAlignment: MainAxisAlignment.end,
//                      children: <Widget>[
//                        Container(
//                          height: 24.7,
//                          child: Center(
//                            child: Text(
//                              course.getTitle(),
//                              textAlign: TextAlign.center,
//                              style: Constants.coursesPageCourseTitlesTextStyle,
//                              overflow: TextOverflow.ellipsis,
//                              softWrap: false,
//                              maxLines: 2,
//                            ),
//                          ),
//                        ),
//                        GradientProgressWidget(
//                          value: course.getProgress(),
//                          height: radiusDouble,
//                          blackLineTop: false,
//                          circularRadius: radius,
//                          constantZero: !(course.isPublished()),
//                        ),
//                      ],
//                    )
//                  : _getComingSoonOverlay(course: course),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Center(
                    child: Text(
                      course.getTitle(),
                      textAlign: TextAlign.center,
                      style: Constants.coursesPageCourseTitlesTextStyle,
                      overflow: TextOverflow.ellipsis,
                      softWrap: false,
                      maxLines: 1,
                    ),
                  ),
                  const  SizedBox(height: 2,),
                  GradientProgressWidget(
                    value: course.getProgress(),
                    height: radiusDouble,
                    blackLineTop: false,
                    circularRadius: radius,
                    constantZero: !(course.isPublished()),
                  ),
                ],
              )
            ]),
          ),
        ),
      ),
    );
  }

  Widget _getComingSoonOverlay({course}) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      decoration: BoxDecoration(
        borderRadius: borderRadius,
        gradient: LinearGradient(
          colors: [
            Color.alphaBlend(
              Constants.courseCardColor.withOpacity(0.8),
              Colors.white.withOpacity(0.5),
            ),
            Color.alphaBlend(
              Constants.courseCardColor.withOpacity(0.45),
              Colors.white70.withOpacity(0.7),
            )
          ],
          begin: const Alignment(-0.2, -0.2),
          end: const Alignment(0.06, 0.6),
        ),
      ),
      child: const Padding(
        padding: const EdgeInsets.all(8.0),
        child: const Text(
          "Coming soon...",
          style: const TextStyle(
            fontFamily: Constants.fontName,
            fontWeight: FontWeight.w500,
            fontSize: 14,
            color: Constants.darkFontColor,
          ),
          textAlign: TextAlign.left,
        ),
      ),
    );
  }
}
