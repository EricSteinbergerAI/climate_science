import 'package:climate_science/backend/content/Course.dart';
import 'package:climate_science/backend/content/Topic.dart';

class TopicProgress {
  Topic _topic;

  TopicProgress({Topic topic}) {
    this._topic = topic;
  }

  double getProgress() {
    double progress = 0.0;
    final List<Course> courses = this._topic.getCourses();
    for (Course c in courses) {
      progress += c.getProgress();
    }
    return progress / courses.length;
  }

  bool isCompleted() {
    return this.getProgress() == 1;
  }
}
