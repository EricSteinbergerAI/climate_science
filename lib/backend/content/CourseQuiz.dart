import 'dart:async';
import 'dart:convert';
import 'dart:io' as io;

import 'package:climate_science/backend/content/_DownloadableProgressableContent.dart';
import 'package:climate_science/backend/content/progress/QuizProgress.dart';

import 'Course.dart';
import 'LevelOfDetail.dart';
import 'Topic.dart';
import 'modules/article/Reference.dart';
import 'modules/quiz/ChoiceQuizModule.dart';
import 'modules/quiz/MatchingQuizModule.dart';
import 'modules/quiz/QuizModuleBase.dart';

class CourseQuiz extends DownloadableProgressableContent {
  String _quizId;
  QuizProgress _progress;
  Topic topic;
  Course course;
  LevelOfDetail levelOfDetail;
  String appSupportDir;

  List<QuizModuleBase> moduleList;
  Map<String, Reference> references;

  CourseQuiz({
    this.appSupportDir,
  }) : super("quiz") {
    this._progress = QuizProgress(quiz: this);
  }

  @override
  Future init() async {
    await super.init();
    this._quizId = '${levelOfDetail.asStr()}-Quiz';
  }

  String getDir() {
    return '$appSupportDir/content/${course.getCourseID()}/$_quizId';
  }

  String getRemoteDir() {
    return '${course.getCourseID()}/$_quizId';
  }

  String getDocument() {
    return '${course.getCourseID()}/$_quizId-quiz';
  }

  bool isLoaded() {
    return moduleList != null;
  }

  Future<void> loadFromDisk() async {
    final jsonFile = io.File('${getDir()}/$jsonContentFileName.json');

    // ------------------------- Load Modules -------------------------
    List<QuizModuleBase> tempModuleList = List();
    Map<String, dynamic> quizDict = json.decode(jsonFile.readAsStringSync());
    for (var moduleDict in quizDict["modules"]) {
      String moduleType = moduleDict["moduleType"];

      // if image, download
      if (moduleType == "choiceQuiz") {
        tempModuleList.add(ChoiceQuizModule(
          quiz: this,
          question: moduleDict["question"],
          questionId: moduleDict["questionId"],
          optionNames: moduleDict["options"].cast<String>(),
          feedbackMsg: moduleDict["feedbackMsg"],
          correctAnswerName: moduleDict["correctAnswer"],
          imgName: moduleDict["picture"],
        ));
      } else if (moduleType == "matchQuiz") {
        tempModuleList.add(MatchingChallengeModule(
          quiz: this,
          question: moduleDict["question"],
          questionId: moduleDict["questionId"],
          feedbackMsg: moduleDict["feedbackMsg"],
          categories: moduleDict["categories"],
          options: moduleDict["options"],
        ));
      } else {
        throw Exception("Unsupported Quiz Module Type: $moduleType");
      }
    }
    // ----------------------- Load References ------------------------
    Map<String, Reference> tempReference = quizDict['refs']
        .asMap()
        .map((id, data) => MapEntry(
            data["name"],
            Reference(
              name: data["name"],
              url: data["url"],
              refNumber: id,
            )))
        .cast<String, Reference>();

    // ---------- Set LOD, Course, and Topic in all modules -----------
    for (QuizModuleBase m in tempModuleList) {
      m.setLevelOfDetail(levelOfDetail: levelOfDetail);
      m.setTopic(topic: topic);
      m.setCourse(course: course);
    }

    // -------------------------- Set things --------------------------
    moduleList = tempModuleList;
    references = tempReference;

    // ------------------------ Load Progress -------------------------
    await loadProgress();
  }

  String getQuizId() {
    return _quizId;
  }

  String getTitle() {
    return this.course.getTitle();
  }

  Future loadProgress() async {
    if (isLoaded()) {
      this._progress.loadFromDisk();
    }
  }

  double getProgress() {
    return this._progress.getProgress();
  }

  void resetProgress() {
    this._progress.resetProgress();
  }

  void resetModuleProgress({QuizModuleBase question}) {
    this._progress.resetModuleProgress(question: question);
  }

  bool isCompleted() {
    return this._progress.isCompleted();
  }

  void registerSolved({QuizModuleBase question}) {
    question.registerChecked();
    this._progress.registerSolved(
          questionId: question.getQuestionId(),
          correct: question.isCorrect(),
        );
  }

  void setTopic({Topic topic}) {
    this.topic = topic;
  }

  void setCourse({Course course}) {
    this.course = course;
  }

  void setLevelOfDetail({LevelOfDetail levelOfDetail}) {
    this.levelOfDetail = levelOfDetail;
  }
}
