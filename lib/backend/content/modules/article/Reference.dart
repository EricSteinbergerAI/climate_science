class Reference {
  int _refNumber;
  String _name;
  String _url;

  Reference({int refNumber, String url, String name}) {
    this._refNumber = refNumber;
    this._url = url;
    this._name = name;
  }

  String getName() {
    return this._name;
  }

  String getURL() {
    return this._url;
  }

  int getNumber() {
    return this._refNumber;
  }
}
