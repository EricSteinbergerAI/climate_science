import 'package:climate_science/backend/content/CourseQuiz.dart';

import 'QuizModuleBase.dart';

class MatchingChallengeModule extends QuizModuleBase {
  String _question;
  List<MatchCategory> _categories;
  List<MatchOption> _options; // can be any number from n_categories to infinity

  // backend state
  Map<MatchCategory, MatchOption> userGuesses;

  MatchingChallengeModule({
    String question,
    List<dynamic> categories,
    List<dynamic> options,
    String questionId,
    String feedbackMsg,
    CourseQuiz quiz,
  }) : super(questionId, feedbackMsg, quiz) {
    this._question = question;

    this._options = options
        .map((optionDict) => MatchOption(
              name: optionDict["name"],
              imgName: optionDict["picture"],
              optionId: optionDict["optionId"],
            ))
        .toList()
        .cast<MatchOption>();

    this._categories = categories
        .map((categoryDict) => MatchCategory(
              name: categoryDict["name"],
              imgName: categoryDict["picture"],
              correctOption: _options.firstWhere(
                  (o) => o.getOptionId() == categoryDict["correctAnswerId"]),
            ))
        .toList()
        .cast<MatchCategory>();
  }

  String getQuestion() {
    return this._question;
  }

  List<MatchCategory> getCategories() {
    return this._categories;
  }

  List<MatchOption> getOptions() {
    return this._options;
  }

  String getOptionImgPath({int optionId}) {
    return this._options[optionId].getImgName();
  }

  String getCategoryImgPath({int categoryId}) {
    return this._categories[categoryId].getImgName();
  }

  void registerGuess({MatchCategory category, MatchOption option}) {
    for (final c in this._categories) {
      if (this.userGuesses[c] == option) {
        this.userGuesses[c] = null;
      }
    }
    this.userGuesses[category] = option;
  }

  @override
  bool isCorrect() {
    if (!this.isChecked()) {
      return false;
    }
    for (final c in this._categories) {
      if (!c.isCorrect(this.userGuesses[c])) {
        return false;
      }
    }
    return true;
  }

  @override
  void internalResetState() {
    this.userGuesses = Map<MatchCategory, MatchOption>();
  }

  @override
  bool attemptIsComplete() {
    for (final c in this._categories) {
      if (this.userGuesses[c] == null) {
        return false;
      }
    }
    return true;
  }
}

class MatchCategory {
  String _name;
  String _imgName;
  MatchOption _correctOption;

  MatchCategory({
    String name,
    String imgName,
    MatchOption correctOption,
  }) {
    this._name = name;
    this._imgName = imgName;
    this._correctOption = correctOption;
  }

  bool isCorrect(MatchOption option) {
    return option == _correctOption;
  }

  String getName() {
    return this._name;
  }

  String getImgName() {
    return this._imgName;
  }

  MatchOption getCorrectAnswer() {
    return this._correctOption;
  }
}

class MatchOption {
  String _name;
  String _imgName;
  String _optionId;

  MatchOption({
    String name,
    String imgName,
    String optionId,
  }) {
    this._optionId = optionId;
    this._imgName = imgName;
  }

  String getImgName() {
    return this._imgName;
  }

  String getOptionId() {
    return this._optionId;
  }

  String getName() {
    return this._name;
  }
}
