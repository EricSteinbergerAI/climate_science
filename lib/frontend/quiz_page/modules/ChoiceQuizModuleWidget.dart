import 'dart:collection';
import 'dart:io';

import 'package:climate_science/backend/content/modules/quiz/ChoiceQuizModule.dart';
import 'package:climate_science/frontend/common/Constants.dart';
import 'package:flutter/material.dart';

class ChoiceQuizModuleWidget extends StatefulWidget {
  final ChoiceQuizModule _quizModule;

  ChoiceQuizModuleWidget(this._quizModule);

  @override
  _ChoiceQuizModuleWidgetState createState() =>
      _ChoiceQuizModuleWidgetState();
}

class _ChoiceQuizModuleWidgetState extends State<ChoiceQuizModuleWidget> {
  void _handleValueChanged({ChoiceOption option}) {
    if (!widget._quizModule.isChecked())
      setState(() {
        widget._quizModule.userGuess = option;
      });
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[_buildContent(context)],
          )),
    );
  }

  Widget _buildContent(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 0, 0, 8),
            child: Column(children: <Widget>[
              _buildImage(),
              Text(
                widget._quizModule.getQuestion(),
                style: Constants.articleBodyTextStyle,
              ),
            ]),
          ),
          Divider(
            color: Colors.black26,
          ),
          Center(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(0, 15, 0, 25),
              child: Column(
                children: widget._quizModule
                    .getOptions()
                    .asMap()
                    .map((i, option) => MapEntry(
                        i,
                        Padding(
                          padding: const EdgeInsets.fromLTRB(0, 12, 0, 0),
                          child: _buildOption(
                            option: option,
                            letter: "ABCDEFGHJKLMNOPQ"[i],
                          ),
                        )))
                    .values
                    .toList(),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildImage() {
    if (widget._quizModule.getImgName() == null) return Container();
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 18, 0, 18),
      child: Image.file(
        File(
          '${widget._quizModule.quiz.getDir()}/${widget._quizModule
              .getImgName()}',
        ),
      ),
    );
  }

  Widget _buildOption({ChoiceOption option, String letter}) {
    final colors = _getChoiceButtonColors(
      option: option,
    );
    if (option == this.widget._quizModule.userGuess ||
        (this.widget._quizModule.isChecked() &&
            option == this.widget._quizModule.getCorrectAnswer())) {
      return FlatButton(
        shape: const RoundedRectangleBorder(
          borderRadius: const BorderRadius.all(Radius.circular(30.0)),
        ),
        child:
            _getButtonContent(option: option, letter: letter, colors: colors),
        onPressed: () {
          if (!widget._quizModule.isChecked()) {
            _handleValueChanged(option: option);
          }
        },
        color: colors["background"],
      );
    } else {
      return OutlineButton(
        shape: const RoundedRectangleBorder(
          borderRadius: const BorderRadius.all(Radius.circular(30.0)),
        ),
        borderSide: BorderSide(color: colors["border"], width: 1.3),
        highlightedBorderColor: colors["border"].withAlpha(188),
        child:
            _getButtonContent(option: option, letter: letter, colors: colors),
        onPressed: () {
          if (!widget._quizModule.isChecked()) {
            _handleValueChanged(option: option);
          }
        },
        color: colors["background"],
      );
    }
  }

  Widget _getButtonContent({
    ChoiceOption option,
    String letter,
    Map<String, Color> colors,
  }) {
    return SizedBox(
      width: double.infinity,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(0, 12, 0, 12),
        child: Row(
          children: <Widget>[
            Text(
              letter,
              style: TextStyle(
                color: colors["font"],
                fontSize: 16,
                fontFamily: Constants.fontName,
                fontWeight: FontWeight.w900,
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 0, 14, 0),
              child: Container(
                height: 22,
                child: VerticalDivider(
                  color: colors["font"],
                  thickness: 1,
                ),
              ),
            ),
            Expanded(
              child: Container(
                child: Text(
                  option.getName(),
                  style: TextStyle(
                    color: colors["font"],
                    fontSize: 16,
                    fontFamily: Constants.fontName,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Map<String, Color> _getChoiceButtonColors({ChoiceOption option}) {
    if (this.widget._quizModule.isChecked()) {
      if (option == widget._quizModule.getCorrectAnswer()) {
        return HashMap.fromEntries([
          const MapEntry("font", Colors.white),
          const MapEntry("border", Colors.black87),
          MapEntry(
              "background", Constants.finalQuizBorderColor.withOpacity(0.8)),
        ]);
      } else if (option == this.widget._quizModule.userGuess) {
        return HashMap.fromEntries([
          const MapEntry("font", Colors.black54),
          const MapEntry("border", Colors.black54),
          const MapEntry("background", Colors.black26),
        ]);
      }
      return HashMap.fromEntries([
        const MapEntry("font", Colors.black38),
        const MapEntry("border", Colors.black38),
        const MapEntry("background", Colors.transparent),
      ]);
    }

    if (option == this.widget._quizModule.userGuess) {
      return HashMap.fromEntries([
        const MapEntry("font", Colors.white),
        const MapEntry("border", Constants.finalQuizBorderColor),
        const MapEntry("background", Constants.finalQuizBorderColor),
      ]);
    }

    return HashMap.fromEntries([
      const MapEntry("font", Colors.black87),
      const MapEntry("border", Colors.black87),
      const MapEntry("background", Colors.transparent),
    ]);
  }
}
