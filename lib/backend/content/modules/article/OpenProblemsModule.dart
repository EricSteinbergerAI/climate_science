import 'ContentModuleBase.dart';

class OpenProblemsModule extends ContentModuleBase {
  String title;
  String description;
  int brainScore;
  int moneyScore;
  int societyScore;

  // each entry contains keys: "name", "link"
  List<Map<String, dynamic>> furtherReading;

  OpenProblemsModule({
    this.title,
    this.description,
    this.brainScore,
    this.moneyScore,
    this.societyScore,
    this.furtherReading,
  });
}
