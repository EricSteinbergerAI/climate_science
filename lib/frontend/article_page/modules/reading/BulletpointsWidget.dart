import 'package:climate_science/backend/content/modules/article/BulletpointModule.dart';
import 'package:climate_science/frontend/common/Constants.dart';
import 'package:flutter/material.dart';

class BulletpointsModuleWidget extends StatelessWidget {
  final BulletpointModule module;
  final bool isActive;

  BulletpointsModuleWidget(this.module, {this.isActive = true});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 2, 0, 2),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: isActive
            ? module.getPoints().map(_buildActiveItem).toList()
            : module.getPoints().map(_buildInactiveItem).toList(),
      ),
    );
  }

  Widget _buildActiveItem(String x) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 4, 0, 4),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          const Text(
            '• ',
            textAlign: TextAlign.left,
            style: Constants.articleBodyTextStyle,
          ),
          Expanded(
            child: Text(
              x,
              textAlign: TextAlign.left,
              style: Constants.articleBodyTextStyle,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildInactiveItem(String x) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 4, 0, 4),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Text(
            '• ',
            textAlign: TextAlign.left,
            style: Constants.articleBodyTextStyle
                .copyWith(color: Constants.inactiveColor),
          ),
          Flexible(
              fit: FlexFit.loose,
              child: LayoutBuilder(
                builder: (context, constraints) {
                  return Container(
                    height: Constants.articleBodyTextStyle.fontSize,
                    width: constraints.constrainWidth() / 3,
                    color: Constants.inactiveColor,
                  );
                },
              )),
        ],
      ),
    );
  }
}
