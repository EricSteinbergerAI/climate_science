import 'package:climate_science/backend/content/Chapter.dart';
import 'package:climate_science/backend/content/CourseQuiz.dart';
import 'package:climate_science/backend/content/Topic.dart';
import 'package:climate_science/backend/content/progress/CourseProgress.dart';

import 'LevelOfDetail.dart';

class Course {
  String _courseId;
  String _name;
  String _summary;
  Map<String, LevelOfDetail> _levelsOfDetail;
  bool _isPublished;

  CoursePrgress _courseProgress;

  Topic topic;

  Course({
    String courseId,
    String name,
    String summary,
    bool isPublished,
    Map<String, LevelOfDetail> levelsOfDetail,
  }) {
    this._courseId = courseId;
    this._name = name;
    this._summary = summary;
    this._levelsOfDetail = levelsOfDetail;
    this._isPublished = isPublished;

    this._courseProgress = CoursePrgress(course: this);

    for (var lod in levelsOfDetail.values) {
      lod.setCourse(course: this);
    }
  }

  Future init() async {
    for (var lod in _levelsOfDetail.values) {
      lod.setCourse(course: this);
    }
    await Future.wait(
        this._levelsOfDetail.values.map((lod) => lod.init()).toList());
  }

//  Future updateFromRemote() async {
//    await Future.wait(this
//        ._levelsOfDetail
//        .values
//        .map((lod) => lod.updateFromRemote())
//        .toList());
//  }

  bool isPublished() {
    return _isPublished;
  }

  double getProgress() {
    return this._courseProgress.getProgress();
  }

  bool isCompleted() {
    return this._courseProgress.isCompleted();
  }

  String getCourseID() {
    return this._courseId;
  }

  String getTitle() {
    return this._name;
  }

  String getSummary() {
    return this._summary;
  }

  List<Chapter> getChapterList(int levelOfDetail) {
    return this._levelsOfDetail[levelOfDetail].getChapters();
  }

  CourseQuiz getQuiz(int levelOfDetail) {
    return this._levelsOfDetail[levelOfDetail].getQuiz();
  }

  Map<String, LevelOfDetail> getLODs() {
    return this._levelsOfDetail;
  }

  LevelOfDetail getBasic() {
    return this._levelsOfDetail[LevelOfDetail.BASICS];
  }

  LevelOfDetail getAdvanced() {
    return this._levelsOfDetail[LevelOfDetail.ADVANCED];
  }

  void setTopic({topic}) {
    this.topic = topic;
    for (var lod in _levelsOfDetail.values) {
      lod.setTopic(topic: topic);
    }
  }
}
