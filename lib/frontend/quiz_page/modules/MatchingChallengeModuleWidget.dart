import 'dart:io';

import 'package:climate_science/backend/content/modules/quiz/MatchingQuizModule.dart';
import 'package:climate_science/frontend/common/Constants.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';

class MatchingChallengeModuleWidget extends StatefulWidget {
  final MatchingChallengeModule _quizModule;

  MatchingChallengeModuleWidget(this._quizModule);

  @override
  _MatchingChallengeModuleWidgetState createState() =>
      _MatchingChallengeModuleWidgetState();
}

class _MatchingChallengeModuleWidgetState
    extends State<MatchingChallengeModuleWidget> {
  static const double _boxRadius = 12;
  static const BorderRadius borderRadius =
      BorderRadius.all(Radius.circular(_boxRadius));

  // Stores which option is currently at this space, if any.
  List<MatchOption> _optionSpaces;
  double _cardWidth;

  @override
  initState() {
    super.initState();
    _optionSpaces = new List<MatchOption>.from(widget._quizModule.getOptions());
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(10, 25, 10, 8),
                child: Text(
                  widget._quizModule.getQuestion(),
                  style: Constants.articleBodyTextStyle,
                ),
              ),
              const Divider(),
              widget._quizModule.isChecked()
                  ? _buildChecked()
                  : _buildUnchecked(),
            ],
          )),
    );
  }

  Widget _buildUnchecked() {
    return Column(
      children: <Widget>[
        LayoutBuilder(
          builder: (context, constraints) {
            _cardWidth = constraints.maxWidth / 4;
            return Column(
              children: <Widget>[
                Row(
                  children: widget._quizModule
                      .getCategories()
                      .map(
                        (cat) => _buildCategory(
                          category: cat,
                          width: constraints.maxWidth / 4,
                        ),
                      )
                      .toList(),
                ),
              ],
            );
          },
        ),
        Container(
          padding: const EdgeInsets.only(top: 20),
          child: GridView.count(
            crossAxisCount: 4,
            shrinkWrap: true,
            children: _optionSpaces
                .asMap()
                .entries
                .map(
                  (e) => _buildOptionSpace(
                    optionPlaced: e.value,
                    optionIndex: e.key,
                    bgColor: const Color.fromARGB(255, 241, 241, 241),
                  ),
                )
                .toList(),
            physics: NeverScrollableScrollPhysics(),
          ),
        )
      ],
    );
  }

  Widget _buildChecked() {
    return Column(
      children: <Widget>[
        LayoutBuilder(
          builder: (context, constraints) {
            _cardWidth = constraints.maxWidth / 4;
            return Column(
              children: <Widget>[
                Row(
                  children: widget._quizModule
                      .getCategories()
                      .map(
                        (cat) => _buildCategoryChecked(
                          category: cat,
                          width: constraints.maxWidth / 4,
                        ),
                      )
                      .toList(),
                ),
              ],
            );
          },
        ),
      ],
    );
  }

  Widget _buildCategory({MatchCategory category, double width}) {
    return Container(
      decoration: const BoxDecoration(
        borderRadius: borderRadius,
      ),
      width: width,
      child: Column(
        children: <Widget>[
          Container(
            width: width,
            height: width * 1.618,
            child: Card(
              shape: const RoundedRectangleBorder(
                borderRadius: borderRadius,
              ),
              color: Constants.courseCardColor,
              child: ClipRRect(
                borderRadius: borderRadius,
                child: Stack(
                  children: <Widget>[
                    Image.file(
                      File(
                        '${widget._quizModule.quiz.getDir()}/${category.getImgName()}',
                      ),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Center(
                          child: Text(
                            category.getName(),
                            textAlign: TextAlign.center,
                            style: Constants.coursesPageCourseTitlesTextStyle,
                            overflow: TextOverflow.ellipsis,
                            softWrap: false,
                            maxLines: 1,
                          ),
                        ),
                        const SizedBox(height: 2)
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
          Container(
              width: width,
              height: width,
              child: _buildOptionSpace(
                  optionPlaced: widget._quizModule.userGuesses[category],
                  category: category,
                  bgColor: const Color.fromARGB(255, 248, 248, 248))),
        ],
      ),
    );
  }

  Widget _buildCategoryChecked({MatchCategory category, double width}) {
    return Container(
      width: width,
      child: Column(
        children: <Widget>[
          Card(
            shape: const RoundedRectangleBorder(
              borderRadius: borderRadius,
            ),
            color: Constants.inChapter,
            child: ClipRRect(
              borderRadius: borderRadius,
              child: Image.file(
                File(
                  '${widget._quizModule.quiz.getDir()}/${category.getImgName()}',
                ),
              ),
            ),
          ),
          Container(
            width: width,
            //height: width,
            child: Padding(
              padding: const EdgeInsets.all(5.0),
              child: _buildCheckedOption(
                guess: widget._quizModule.userGuesses[category],
                correct: category.getCorrectAnswer(),
                isCorrect: category.isCorrect(
                  widget._quizModule.userGuesses[category],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildOptionSpace({
    MatchOption optionPlaced,
    MatchCategory category,
    int optionIndex = -1,
    Color bgColor,
  }) {
    return Container(
        child: DragTarget(
      builder: (context, List<MatchOption> candidateData, rejectedData) {
        return Padding(
          padding: const EdgeInsets.all(5.0),
          child: optionPlaced == null
              ? Card(
                  shape: const RoundedRectangleBorder(
                    borderRadius: borderRadius,
                  ),
                  elevation: 0,
                  child: DottedBorder(
                    color: Colors.black12,
                    borderType: BorderType.RRect,
                    radius: const Radius.circular(_boxRadius),
                    child: Container(),
                  ),
                  color: bgColor,
                )
              : _buildOption(
                  option: optionPlaced,
                ),
        );
      },
      onWillAccept: (data) {
        return optionPlaced == null ? true : false;
      },
      onAccept: (optionSelected) {
        // Remove option from the old place .
        if (this._optionSpaces.contains(optionSelected)) {
          _optionSpaces[_optionSpaces.indexOf(optionSelected)] = null;
        }
        if (widget._quizModule.userGuesses.containsValue(optionSelected)) {
          widget._quizModule.registerGuess(
              category: widget._quizModule.userGuesses.keys.firstWhere(
                  (k) => widget._quizModule.userGuesses[k] == optionSelected),
              option: null);
        }
        // And move to the new one.
        if (category != null) {
          widget._quizModule.registerGuess(
            category: category,
            option: optionSelected,
          );
        }
        if (optionIndex != -1) {
          _optionSpaces[optionIndex] = optionSelected;
        }
        setState(() {});
      },
    ));
  }

  Widget _buildOption({MatchOption option}) {
    return Container(
      width: _cardWidth,
      height: _cardWidth,
      child: Draggable<MatchOption>(
        feedback: Container(
            width: 50,
            height: 50,
            child: Image.file(File(
              '${widget._quizModule.quiz.getDir()}/${option.getImgName()}',
            ))),
        child: Container(
          decoration: const BoxDecoration(
            borderRadius: borderRadius,
            boxShadow: [
              const BoxShadow(color: Constants.shadow, blurRadius: 4)
            ],
          ),
          child: ClipRRect(
            borderRadius: borderRadius,
            child: Image.file(
              File(
                '${widget._quizModule.quiz.getDir()}/${option.getImgName()}',
              ),
            ),
          ),
        ),
        data: option,
      ),
    );
  }

  Widget _buildCheckedOption(
      {MatchOption guess, MatchOption correct, bool isCorrect}) {
    return Column(
      children: <Widget>[
        Stack(
          children: <Widget>[
            // User guess:
            Container(
              width: _cardWidth,
              height: _cardWidth,
              alignment: Alignment.center,
              child: ColorFiltered(
                colorFilter: ColorFilter.mode(
                  Colors.grey,
                  BlendMode.saturation,
                ),
                child: Image.file(
                  File(
                    '${widget._quizModule.quiz.getDir()}/${guess.getImgName()}',
                  ),
                  width: _cardWidth,
                  height: _cardWidth,
                ),
              ),
            ),
            Container(
                width: _cardWidth,
                height: _cardWidth,
                alignment: Alignment.topRight,
                child: isCorrect ? Icon(Icons.check) : Icon(Icons.clear))
          ],
        ),
        Container(
          width: _cardWidth,
          height: _cardWidth,
          child: ClipRRect(
            borderRadius: borderRadius,
            child: Image.file(
              File(
                '${widget._quizModule.quiz.getDir()}/${correct.getImgName()}',
              ),
            ),
          ),
        ),
      ],
    );
  }
}
