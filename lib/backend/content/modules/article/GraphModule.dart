import 'ContentModuleBase.dart';

class GraphModule extends ContentModuleBase {
  List<GraphDataPoint> data;

  String yAxisLabel;
  bool xAxisIsInt;
  String xAxisLabel;

  GraphModule({
    List<num> x,
    List<num> y,
    String xAxisType,
    this.yAxisLabel,
    this.xAxisLabel,
  }) {
    data = Iterable<num>.generate(x.length)
        .map((i) => GraphDataPoint(x[i], y[i]))
        .toList();
    if (xAxisType == "int") {
      this.xAxisIsInt = true;
    } else {
      this.xAxisIsInt = false;
    }
  }
}

class GraphDataPoint {
  num x, y;

  GraphDataPoint(this.x, this.y);
}
