class ClimateScienceUser {
  final String uid;
  final String email;
  final bool isMinor;
  bool emailVerified;

  ClimateScienceUser({this.uid, this.email, this.isMinor, this.emailVerified});
}
