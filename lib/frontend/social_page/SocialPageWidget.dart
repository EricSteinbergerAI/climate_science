import 'package:climate_science/frontend/article_page/modules/reading/SharingModuleWidget.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class SocialPageWidget extends StatefulWidget {
  SocialPageWidget({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _SocialPageWidgetState createState() => _SocialPageWidgetState();
}

class _SocialPageWidgetState extends State<SocialPageWidget> {
  @override
  Widget build(BuildContext context) {
    return Padding(padding: EdgeInsets.all(30), child: SharingModuleWidget());
  }
}
