import 'package:climate_science/backend/content/CourseQuiz.dart';

import 'QuizModuleBase.dart';

class ChoiceQuizModule extends QuizModuleBase {
  String _question;
  List<ChoiceOption> _options;
  ChoiceOption _correctAnswer;
  String _imgName;

  // backend state
  ChoiceOption userGuess;

  ChoiceQuizModule({
    String question,
    List<String> optionNames,
    String correctAnswerName,
    String questionId,
    String feedbackMsg,
    String imgName,
    CourseQuiz quiz,
  }) : super(questionId, feedbackMsg, quiz) {
    this._question = question;
    this._options = optionNames
        .map((optionName) => ChoiceOption(name: optionName))
        .toList()
        .cast<ChoiceOption>();
    this._correctAnswer =
        _options.firstWhere((o) => o.getName() == correctAnswerName);
    this._imgName = imgName;
  }

  String getQuestion() {
    return this._question;
  }

  List<ChoiceOption> getOptions() {
    return this._options;
  }

  ChoiceOption getCorrectAnswer() {
    return this._correctAnswer;
  }

  @override
  bool isCorrect() {
    return this.isChecked() && userGuess == _correctAnswer;
  }

  String getImgName() {
    return this._imgName;
  }

  @override
  void internalResetState() {
    this.userGuess = null;
  }

  @override
  bool attemptIsComplete() {
    return userGuess != null;
  }
}

class ChoiceOption {
  String _name;

  ChoiceOption({
    String name,
  }) {
    this._name = name;
  }

  String getName() {
    return this._name;
  }
}
