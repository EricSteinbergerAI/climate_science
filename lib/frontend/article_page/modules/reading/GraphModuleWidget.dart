import 'package:charts_flutter/flutter.dart' as charts;
import 'package:climate_science/backend/content/modules/article/GraphModule.dart';
import 'package:climate_science/frontend/common/Constants.dart';
import 'package:flutter/material.dart';

class GraphModuleWidget extends StatelessWidget {
  final GraphModule _module;
  final List<charts.Series<GraphDataPoint, num>> _seriesList;
  final bool isActive;

  static final List<charts.Series<GraphDataPoint, num>> _fakeContent = [
    charts.Series<GraphDataPoint, num>(
      id: 'Sales',
      colorFn: (_, __) => charts.Color(
        r: Constants.inactiveColor.red,
        g: Constants.inactiveColor.green,
        b: Constants.inactiveColor.blue,
        a: Constants.inactiveColor.alpha,
      ),
      domainFn: (GraphDataPoint t, _) => t.x,
      measureFn: (GraphDataPoint t, _) => t.y,
      data: Iterable<int>.generate(100)
          .toList()
          .map((i) => (i - 30).toDouble() / 50.0)
          .map((i) => GraphDataPoint(i, i * i + 0.3))
          .toList(),
    )
  ];

  GraphModuleWidget(this._module, {this.isActive = true})
      : this._seriesList = [
          charts.Series<GraphDataPoint, num>(
            id: 'Sales',
            colorFn: (_, i) => _getChartColor(
              color: Constants.inChapter,
              id: i.toDouble(),
              n: _module.data.length.toDouble(),
            ),
            domainFn: (GraphDataPoint t, _) => t.x,
            measureFn: (GraphDataPoint t, _) => t.y,
            data: _module.data,
          )
        ];

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(top: 5, bottom: 25),
      height: 340,
      child: isActive ? _buildActive() : _buildInactive(),
    );
  }

  Widget _buildActive() {
    return charts.LineChart(
      _seriesList,
      primaryMeasureAxis: charts.NumericAxisSpec(
          tickProviderSpec:
              charts.BasicNumericTickProviderSpec(zeroBound: false)),
      domainAxis: charts.NumericAxisSpec(
          tickProviderSpec:
              charts.BasicNumericTickProviderSpec(zeroBound: false)),
      animate: true,
      behaviors: [
        charts.ChartTitle(_module.yAxisLabel,
            behaviorPosition: charts.BehaviorPosition.start,
            maxWidthStrategy: charts.MaxWidthStrategy.truncate,
            titleStyleSpec: Constants.chartTextStyleSpec,
            titleOutsideJustification:
                charts.OutsideJustification.middleDrawArea),
        charts.ChartTitle(_module.xAxisLabel,
            behaviorPosition: charts.BehaviorPosition.bottom,
            maxWidthStrategy: charts.MaxWidthStrategy.truncate,
            titleStyleSpec: Constants.chartTextStyleSpec,
            titleOutsideJustification:
                charts.OutsideJustification.middleDrawArea),
      ],
    );
  }

  Widget _buildInactive() {
    return charts.LineChart(
      _fakeContent,
      animate: false,
    );
  }

  static charts.Color _getChartColor({Color color, double id, double n}) {
    double k = 1;
    return charts.Color(
      r: (color.red * (k + (1 - k) * (1 - (id / n)))).toInt(),
      g: (color.green * (k + (1 - k) * (1 - (id / n)))).toInt(),
      b: (color.blue * (k + (1 - k) * (1 - (id / n)))).toInt(),
      a: color.alpha,
    );
  }
}
