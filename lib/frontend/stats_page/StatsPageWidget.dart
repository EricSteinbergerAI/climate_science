import 'dart:ui';

import 'package:charts_flutter/flutter.dart' as charts;
import 'package:climate_science/backend/content/Topic.dart';
import 'package:climate_science/frontend/common/Constants.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class StatsPageWidget extends StatefulWidget {
  final List<Topic> _topics;

  StatsPageWidget(
    this._topics, {
    Key key,
    this.title,
  }) : super(key: key);
  final String title;

  @override
  _StatsPageWidgetState createState() => _StatsPageWidgetState();
}

class _StatsPageWidgetState extends State<StatsPageWidget> {

  static const scoreStyle = TextStyle(color: Colors.blue, fontSize: 16);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.fromLTRB(0.0, 18, 0.0, 25),
              child: Center(
                child: Text(
                  'Your Progress',
                  style: Constants.pageTitleTextStyle,
                ),
              ),
            ),
            _createStatistics(),
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 21, 0, 0),
              child: _createChartSection(),
            )
          ],
        ));
  }

  Widget _createStatistics() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Total',
          style: Constants.coursesPageTopicTextStyle,
        ),
        Container(
          padding: EdgeInsets.all(4),
          child: Table(children: <TableRow>[
            ///First table row with 3 children
            TableRow(
              children: <Widget>[
                Text('Climate points'),
                Text(
                  'TODO', // TODO
                  style: scoreStyle,
                )
              ],
            ),
            TableRow(
              children: <Widget>[
                Text('Courses completed'),
                Text(
                  '${widget._topics.map((topic) =>
                  topic
                      .getCourses()
                      .where((course) => course.isCompleted())
                      .toList()
                      .length).reduce((a, b) => a + b).toInt().toString()}',
                  style: scoreStyle,
                )
              ],
            ),
            TableRow(
              children: <Widget>[
                Text('Chapters completed'),
                Text(
                  '${widget._topics.map((topic) =>
                  topic
                      .getCourses()
                      .map((course) =>
                      course
                          .getLODs()
                          .values
                          .map((lod) =>
                      lod
                          .getChapters()
                          .where((chapter) => chapter.isCompleted())
                          .toList()
                          .length)
                          .reduce((a, b) => a + b))
                      .toList()
                      .length).reduce((a, b) => a + b).toInt().toString()}',
                  style: scoreStyle,
                )
              ],
            )
          ]),
        ),
      ],
    );
  }

  Widget _createChartSection() {
    return Container(
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
          Text(
            'Percent completed',
            style: Constants.coursesPageTopicTextStyle,
          ),
          Container(
            child: _createChart(),
            height: 250,
          )
        ]));
  }

  Widget _createChart() {
    final staticTicks = <charts.TickSpec<int>>[
      charts.TickSpec(25),
      charts.TickSpec(50),
      charts.TickSpec(75),
      charts.TickSpec(100),
    ];

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: charts.BarChart(
        _createSeries(),
        animate: true,
        vertical: false,
        domainAxis: charts.OrdinalAxisSpec(
          renderSpec: charts.SmallTickRendererSpec(
            labelStyle: Constants.chartTextStyleSpec,
//            labelRotation: -45,
          ),
        ),
        primaryMeasureAxis: charts.NumericAxisSpec(
          tickProviderSpec: charts.StaticNumericTickProviderSpec(
            staticTicks,
          ),
        ),
        defaultInteractions: false,
        behaviors: [
          charts.SelectNearest(),
          charts.DomainHighlighter(),
          charts.ChartTitle(
            '% complete',
            titleStyleSpec: charts.TextStyleSpec(fontSize: 15),
            behaviorPosition: charts.BehaviorPosition.bottom,
            titleOutsideJustification:
                charts.OutsideJustification.middleDrawArea,
          ),
        ],
      ),
    );
  }

  List<charts.Series<Topic, String>> _createSeries() {
    return [
      charts.Series<Topic, String>(
        id: 'Progresses',
        colorFn: (current, i) => _getColor(i, widget._topics.length),
        domainFn: (Topic topic, _) => topic.getShortName(),
        measureFn: (Topic topic, _) => topic.getProgress() * 100,
        data: widget._topics,
      )
    ];
  }

  charts.Color _getColor(int index, int maxIndex) {
    Color startColor = Colors.cyan[200];
    Color endColor = Colors.lightBlue[600];
    double alpha = index.toDouble() / maxIndex;
    return charts.Color(
        r: (startColor.red * alpha + endColor.red * (1 - alpha)).round(),
        g: (startColor.green * alpha + endColor.green * (1 - alpha)).round(),
        b: (startColor.blue * alpha + endColor.blue * (1 - alpha)).round());
  }
}
