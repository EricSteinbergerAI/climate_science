import 'package:climate_science/backend/content/modules/quiz/ChoiceQuizModule.dart';
import 'package:climate_science/backend/content/modules/quiz/MatchingQuizModule.dart';
import 'package:climate_science/backend/content/modules/quiz/QuizModuleBase.dart';
import 'package:climate_science/frontend/quiz_page/modules/ChoiceQuizModuleWidget.dart';
import 'package:climate_science/frontend/quiz_page/modules/MatchingChallengeModuleWidget.dart';
import 'package:flutter/material.dart';

class QuizModuleBaseWidget extends StatelessWidget {
  final QuizModuleBase quizModuleBase;

  QuizModuleBaseWidget(this.quizModuleBase);

  @override
  Widget build(BuildContext context) {
    if (quizModuleBase is MatchingChallengeModule) {
      return MatchingChallengeModuleWidget(
          quizModuleBase as MatchingChallengeModule);
    } else if (quizModuleBase is ChoiceQuizModule) {
      return ChoiceQuizModuleWidget(quizModuleBase as ChoiceQuizModule);
    }
    throw Exception("Unkown quiz type" + quizModuleBase.toString());
  }
}
