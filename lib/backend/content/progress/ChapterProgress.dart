import 'dart:convert';
import 'dart:io' as io;

import 'package:climate_science/backend/content/Chapter.dart';
import 'package:climate_science/backend/content/modules/article/challenges/ChallengeModuleBase.dart';

import '../_PrimaryProgress.dart';

class ChapterProgress extends PrimaryProgress {
  Chapter chapter;
  Map<String, bool> _state;

  ChapterProgress({Chapter chapter}) {
    this.chapter = chapter;
    this._state = null;
  }

  @override
  Future loadFromDisk() async {
    final file = io.File(getProgressDir());
    if (file.existsSync()) {
      this._state = json.decode(file.readAsStringSync()).cast<String, bool>();
    } // if not existing, return allFalse map
    else {
      this._state = new Map();
      this._state["wasEverOpened"] = false;
    }
  }

  @override
  Future storeToDisk() async {
    final file = io.File(getProgressDir());
    file.createSync(recursive: true);
    file.writeAsStringSync(json.encode(this._state));
  }

  @override
  String getProgressDir() {
    return '${chapter.getDir()}/progress.json';
  }

  void registerChapterOpened() {
    this._state["wasEverOpened"] = true;
    this.storeToDisk();
  }

  double getProgress() {
    if (_state == null) {
      return 0;
    }

    final List<ChallengeModuleBase> challenges = chapter
        .getModuleList()
        .where((m) => m is ChallengeModuleBase)
        .toList()
        .cast<ChallengeModuleBase>();
    if (challenges.length > 0) {
      return (challenges.where((c) => c.isAttempted()).toList().length /
          challenges.length);
    }

    // Chapter contains no challenges
    if (this._state["wasEverOpened"]) {
      return 1;
    }

    return 0;
  }

  bool isCompleted() {
    return this.getProgress() == 1;
  }
}
