import 'dart:math';

import 'package:climate_science/backend/content/modules/article/challenges/CheckboxChallengeModule.dart';
import 'package:climate_science/frontend/common/Constants.dart';
import 'package:flutter/material.dart';

class CheckBoxChallengeModuleWidget extends StatefulWidget {
  final CheckboxChallengeModule content;
  final Function _rebuildCallback;
  final bool isActive;

  CheckBoxChallengeModuleWidget(this.content, this._rebuildCallback,
      {this.isActive = true});

  @override
  State<StatefulWidget> createState() {
    return CheckBoxChallengeModuleWidgetState();
  }
}

class CheckBoxChallengeModuleWidgetState
    extends State<CheckBoxChallengeModuleWidget> {
  void _checkAnswer() {
    widget.content.registerAttempted();
    setState(() {});
    widget._rebuildCallback();
  }

  @override
  Widget build(BuildContext context) {
    if (!widget.isActive) return Container();
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              widget.content.getQuestion(),
              style: Constants.articleQuestionStyle,
            ),
            Container(),
            Column(
                mainAxisSize: MainAxisSize.min,
                children: widget.content
                    .getOptions()
                    .map((e) => _buildCheckbox(e))
                    .toList()),
            widget.content.isChecked()
                ? _printCorrectAnswer()
                : Center(
                    child: FlatButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(4)),
                    child: Text(
                      'Check answer',
                      style: Constants.articleBodyTextStyle.copyWith(
                        fontWeight: FontWeight.w500,
                        color: Colors.white,
                      ),
                    ),
                    color: Constants.inChapter,
                    onPressed: _checkAnswer,
                  )),
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 7, 15, 35),
              child: Align(
                alignment: Alignment.centerRight,
                child: InkWell(
                  child: Text(
                    'Retry?',
                    style: Constants.articleBodyTextStyle
                        .copyWith(color: Constants.lightText),
                  ),
                  onTap: () {
                    widget.content.reset();
                    setState(() {});
                  },
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _printCorrectAnswer() {
    return Padding(
      padding: const EdgeInsets.only(top:15, bottom: 4),
      child: Center(
        child: Text(
          '${_getCorrectNumber()}/${widget.content
              .getOptions()
              .length} correct',
          style: Constants.articleFeedbackCorrectTextStyle,
          textAlign: TextAlign.center,
        ),
      ),
    );
  }

  int _getCorrectNumber() {
    int correct = 0;
    widget.content.getOptions().forEach((option) {
      if (widget.content.getGuesses().contains(option)) {
        if (widget.content.getCorrectAnswers().contains(option)) {
          correct++;
        }
      } else {
        if (!widget.content.getCorrectAnswers().contains(option)) {
          correct++;
        }
      }
    });
    return max(correct, 0);
  }

  Widget _buildCheckbox(String title) {
    bool isCorrect = widget.content.getCorrectAnswers().contains(title);
    bool isChosen = widget.content.getGuesses().contains(title);
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Checkbox(
          value: widget.content.isChecked()
              ? (isChosen || isCorrect)
              : isChosen,
          activeColor: (widget.content.isChecked() && !isCorrect)
              ? Colors.black12
              : Constants.inChapter,
          onChanged: (bool value) {
            if (!widget.content.isChecked()) {
              widget.content.registerGuess(title);
              setState(() {});
            }
          },
        ),
        Expanded(
            child: Text(
          title,
          maxLines: 55,
        )),
      ],
    );
  }
}
