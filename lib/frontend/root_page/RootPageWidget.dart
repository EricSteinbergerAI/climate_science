import 'package:climate_science/backend/authentication.dart';
import 'package:climate_science/frontend/common/dismissableAlert.dart';
import 'package:climate_science/frontend/home_page/HomePageWidget.dart';
import 'package:climate_science/frontend/loading_page/LoadingPageWidget.dart';
import 'package:climate_science/frontend/login/WelcomePage.dart';
import 'package:climate_science/models/ClimateScienceUser.dart';
import 'package:flutter/material.dart';

enum AuthStatus {
  NOT_DETERMINED,
  NOT_LOGGED_IN,
  LOGGED_IN,
}

class RootPage extends StatefulWidget {
  RootPage({this.auth});

  Auth auth;

  @override
  State<StatefulWidget> createState() => new _RootPageState();
}

class _RootPageState extends State<RootPage> {
  AuthStatus authStatus = AuthStatus.NOT_DETERMINED;
  ClimateScienceUser _csUser;

  @override
  void initState() {
    super.initState();
    tryLoginCallback(); // calling async method without await -- showing wait screen in build method until it is done
  }

  Future tryLoginCallback() async {
    ClimateScienceUser u = await widget.auth.getCurrentUser();
    if (u == null) {
      await logoutCallback();
    } else {
      if (u.isMinor && !u.emailVerified) {
        widget.auth.sendEmailVerification();
        await showDialog(
          context: context,
          builder: (BuildContext context) {
            return _getMinorCantEnterDialog();
          },
        );
        await logoutCallback();
      } else {
        setState(
          () {
            _csUser = u;
            authStatus = AuthStatus.LOGGED_IN;
          },
        );
      }
    }
    Navigator.popUntil(context, ModalRoute.withName('/'));
  }

  Future logoutCallback() async {
    await widget.auth.signOut();
    setState(() {
      authStatus = AuthStatus.NOT_LOGGED_IN;
      _csUser = null;
      Navigator.popUntil(context, ModalRoute.withName('/'));
    });
  }

  Future minorSignUpCallback() async {
    setState(
      () {
        authStatus = AuthStatus.NOT_LOGGED_IN;
        _csUser = null;
      },
    );
    await showDialog(
      context: context,
      builder: (BuildContext context) {
        return _getMinorCantEnterDialog();
      },
    );
    Navigator.popUntil(context, ModalRoute.withName('/'));
  }

  Widget buildWaitingScreen() {
    return LoadingPageWidget("Authenticating...");
  }

  @override
  Widget build(BuildContext context) {
    switch (authStatus) {
      case AuthStatus.NOT_DETERMINED:
        return buildWaitingScreen();
        break;
      case AuthStatus.NOT_LOGGED_IN:
        return WelcomePage(
          auth: widget.auth,
          loginCallback: tryLoginCallback,
          minorSignUpCallback: minorSignUpCallback,
        );
        break;
      case AuthStatus.LOGGED_IN:
        assert(_csUser != null && _csUser.uid.length > 0); // TODO Remove
        return HomePageWidget(
          csUser: _csUser,
          auth: widget.auth,
          logoutCallback: logoutCallback,
        );
        break;
      default:
        throw Exception(
            "Climate Science crashed because login status is not classified. Sorry!");
    }
  }

  Widget _getMinorCantEnterDialog() {
    return getDismissableAlert(
        context: context,
        formText:
            "You can use Climate Science after your parent gave their consent by clicking the link we sent them via email.");
  }
}
