import 'dart:convert';
import 'dart:io' as io;

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:synchronized/synchronized.dart';

class ContentNotFoundError implements Exception {}

class DownloadableProgressableContent {
  final String jsonContentFileName;
  final databaseRef = Firestore.instance;
  final storageRef = FirebaseStorage.instance.ref();
  Lock _lock = Lock();
  bool isDownloading = false;

  Future<void> init() async {
    if (anyVersionDownloaded()) {
      await loadFromDisk();
      await loadProgress();
    }
  }

  DownloadableProgressableContent(this.jsonContentFileName);

  Future<void> doNothingIfLoadedElseUpdateAndLoad(
      {Function setStateCallback}) async {
    try {
      isDownloading = true;
      setStateCallback();
      if (!anyVersionDownloaded() || !isLoaded()) {
        await updateFromRemote();
        await loadFromDisk();
      }
    } finally {
      isDownloading = false;
      setStateCallback();
    }
  }

  Future<void> updateFromRemote() async {
    try {
      await _lock.synchronized(() async {
        if (_downloadAllowedNow()) {
          // -------------------------- Download JSON --------------------------
          Map<String, dynamic> remoteDict;
          await databaseRef
              .document(getDocument())
              .get()
              .then((DocumentSnapshot doc) {
            if (doc.exists) {
              remoteDict = Map<String, dynamic>.from(doc.data);
            } else {
              throw ContentNotFoundError();
            }
          });

          final localPath = getDir();
          final jsonFile = io.File('$localPath/$jsonContentFileName.json');
          // ------------------------- Download Assets -------------------------
          await DownloadableProgressableContent.updateAllAssets(
            localJsonFile: jsonFile,
            remoteAssetsDict: Map<String, dynamic>.from(remoteDict["assets"]),
            storageRef: storageRef,
            remoteDir: getRemoteDir(),
            localPath: localPath,
          );
          // --------------------------- Store JSON ----------------------------
          jsonFile.createSync(recursive: true);
          jsonFile.writeAsStringSync(json.encode(remoteDict));

          // --------------- Block update for the next few hours ---------------
          _writeBlockFile();
        }
      });
    } on io.SocketException catch (e) {
      if (this.anyVersionDownloaded()) {
        return null;
      } else {
        throw io.SocketException("Couldn't download");
      }
    }
  }

  Future<void> loadProgress() async {
    throw UnimplementedError;
  }

  Future<void> loadFromDisk() async {
    // assumes the data exists locally! Does NOT check for it.
    throw UnimplementedError;
  }

  String getDir() {
    throw UnimplementedError;
  }

  String getRemoteDir() {
    throw UnimplementedError;
  }

  String getDocument() {
    throw UnimplementedError;
  }

  bool isLoaded() {
    throw UnimplementedError;
  }

  bool anyVersionDownloaded() {
    return io.File('${getDir()}/$jsonContentFileName.json').existsSync();
  }

  // ----------------- utility ----------------------
  static Future updateAllAssets({
    localJsonFile,
    Map<String, dynamic> remoteAssetsDict,
    StorageReference storageRef,
    String remoteDir,
    String localPath,
  }) async {
    // NOTE: assumes that if localJsonFile exists, then it has a key "assets".
    List<String> fileNames;

    // Only download assets that changed
    if (localJsonFile.existsSync()) {
      Map<String, dynamic> localDict =
          json.decode(localJsonFile.readAsStringSync());

      final localDictAssets = localDict["assets"];
      final r = Map<String, dynamic>.from(remoteAssetsDict);
      r.removeWhere((name, remoteAssetDict) =>
          localDictAssets[name]["updated"] == remoteAssetDict["updated"]);
      fileNames = r.keys.toList();
    } else {
      fileNames = remoteAssetsDict.keys.toList();
    }

    // Download
    await Future.wait(fileNames
        .map((fileName) => downloadFile(
              storageRef,
              savedDir: localPath,
              remoteDir: remoteDir,
              fileName: fileName,
            ))
        .toList()
        .cast<Future<dynamic>>());
  }

  static Future<bool> downloadFile(StorageReference storageRef,
      {String savedDir, String remoteDir, String fileName}) async {
    new io.Directory(savedDir).create(recursive: true);
    final ref = storageRef.child('$remoteDir/$fileName');
    // no need of the file extension, the name will do fine.
    var url = await ref.getDownloadURL();
    await (io.HttpClient())
        .getUrl(Uri.parse(url))
        .then((io.HttpClientRequest request) {
      return request.close();
    }).then((io.HttpClientResponse response) async {
      await response.pipe(io.File('$savedDir/$fileName').openWrite());
    });
    return true;
  }

  bool _downloadAllowedNow() {
    final blockFile = io.File('${getDir()}/blockDwnl.txt');
    if ((!blockFile.existsSync())) {
      return true;
    }
    final last = DateTime.parse(blockFile.readAsStringSync());
    return DateTime.now().difference(last).inSeconds >= 3600;
  }

  void _writeBlockFile() {
    final blockFile = io.File('${getDir()}/blockDwnl.txt');
    blockFile.writeAsStringSync('${DateTime.now()}');
  }
}
