import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';

class LocalImage extends StatelessWidget {
  final String _path;

  LocalImage(this._path);

  Future<File> _getLocalFile() async {
    String filesDir = (await getApplicationSupportDirectory()).path;
    String path = '$filesDir/$_path';
    File f = File(path);
    return f;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: _getLocalFile(),
        builder: (BuildContext context, AsyncSnapshot<File> snapshot) {
          return snapshot.data != null
              ? new Image.file(snapshot.data, fit: BoxFit.fill)
              : new Container();
        });
  }
}
