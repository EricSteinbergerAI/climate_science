import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';

class Constants {
  // Laws
  static const FEEDBACK_LIMIT_PER_CHAPTER = 10;
  static const SECONDS_BETWEEN_UPDATE_CHAPTER = 3600;



  // Design
  static const double HORIZONTAL_PADDING = 15;
  static const Color darkFontColor = Color.fromARGB(255, 77, 77, 77);

  static const Color red = Color(0xFFFF5380);
  static const Color green = Color(0xFF28DF6F);
  static const Color gradientR = Color(0xFFFF9578);

  static const Color gradient2L = Color(0xFFB3D2FF);
  static const Color gradient2R = Color(0xFFDDC5FF);

  static const Color finalQuizBorderColor = red;

  static const Color courseCardColor = Color(0xFFD3E1FF);

  static const Color blue2 = Color(0xFF213333);

  // static const Color red = Color(0xFFFF5380);

  static const Color buttonColor = Color(0xFF5368ff);
  static const Color inChapter = Color(0xFF7990FF);
  static const Color darkInChapter = Color(0xFF3042bf);

  static const Color bg1 = Color(0xFFFCFCFC);
  static const Color bg2 = Color(0xFFFFF9DC);

  static const Color niceColor4 = Color(0xFFCDDCFF);
  static const Color niceColor3 = Color(0xFFC8F8FF);

  static const Color almostWhite = Color(0xFFFDFDFD);
  static const Color almostWhite2 = Color(0xFFEEF1F3);
  static const Color almostalmostWhite = Color(0xFFF8F5FF);

  static const Color inactiveColor = Color(0xFFEEEEEE);

  static const Color nearlyBlue = Color(0xFF00B6F0);
  static const Color nearlyBlack = Color(0xFF213333);
  static const Color nearlyBlack40op = Color(0x66213333);
  static const Color lightGrey = Color(0xFFAAAAAA);
  static const Color grey = Color(0xFF3A5160);

  static const Color lightPurple = Color(0xFFaca3ff);

  static const Color lightText = Color(0xFF4A6572);
  static const Color normalText = Color(0xFF3e5159);
  static const Color darkText = Color(0xFF334354);
  static const Color darkerText = Color(0xFF17262A);

  static const Color shadow = Color(0x33313A44);

  static const fontName = 'WorkSans';
  static const fontName2 = 'Roboto';

  // General
  static const appBarTextStyle = TextStyle(
    fontSize: 17.0,
    color: blue2,
    fontWeight: FontWeight.w600,
    fontFamily: fontName,
  );
  static const pageTitleTextStyle = TextStyle(
    fontSize: 38.0,
    color: darkText,
    fontWeight: FontWeight.w600,
    fontFamily: fontName,
  );

  // Courses Page
  static const coursesPageTopicTextStyle = TextStyle(
    fontSize: 17.5,
    color: Color.fromARGB(255, 100, 100, 100),
    fontWeight: FontWeight.w500,
    fontFamily: fontName,
  );

  static const coursesPageCourseTitlesTextStyle = TextStyle(
    fontSize: 13.5,
    color: darkText,
    fontWeight: FontWeight.w500,
    fontFamily: fontName,
  );

  // Course Page
  static const coursePageCourseTitleTextStyle = TextStyle(
    fontSize: 28.0,
    color: darkText,
    fontWeight: FontWeight.w600,
    fontFamily: fontName,
  );
  static const coursePageChapterNameTextStyle = TextStyle(
    fontSize: 16.0,
    color: darkText,
    fontWeight: FontWeight.w500,
    fontFamily: fontName,
  );
  static const coursePageDownloadTextStyle = TextStyle(
    fontSize: 13.5,
    color: lightText,
    fontWeight: FontWeight.w400,
    fontFamily: fontName2,
  );

  // Article
  static const articleTitleTextStyle = TextStyle(
    fontSize: 25.0,
    color: darkText,
    height: 1.3,
    fontFamily: fontName,
    fontWeight: FontWeight.w500,
  );
  static const articleSectionTitleStyle = TextStyle(
    fontSize: 20.0,
    color: darkText,
    fontFamily: fontName,
    fontWeight: FontWeight.w500,
  );
  static const articleBodyTextStyle = TextStyle(
    fontSize: 16.2,
    color: normalText,
    fontFamily: fontName,
  );
  static const articleBodyBoldTextStyle = TextStyle(
    fontSize: 16.2,
    color: normalText,
    fontFamily: fontName,
    fontWeight: FontWeight.w600,
  );
  static const  articleBodyLinkTextStyle = TextStyle(
    fontSize: 16.2,
    color: buttonColor,
    fontFamily: fontName,
  );
  static const articleBodySmallTextStyle = TextStyle(
    fontSize: 15.0,
    color: normalText,
    fontFamily: fontName,
  );
  static const articleBodySmallBoldTextStyle = TextStyle(
    fontSize: 15.0,
    color: normalText,
    fontFamily: fontName,
    fontWeight: FontWeight.w600,
  );
  static const articleReferenceTextStyle = TextStyle(
    fontSize: 10,
    fontFamily: fontName,
    color: buttonColor,
  );
  static const articleQuestionStyle = TextStyle(
    fontSize: 16.0,
    color: darkText,
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
  );
  static const articleFeedbackNeutralTextStyle = TextStyle(
    fontSize: 14,
    color: darkText,
    fontFamily: fontName,
    fontWeight: FontWeight.w300,
  );
  static const articleFeedbackCorrectTextStyle = TextStyle(
    fontSize: 14,
    color: green,
    fontFamily: fontName,
    fontWeight: FontWeight.w300,
  );
  static const articleFeedbackWrongTextStyle = TextStyle(
    fontSize: 14,
    color: red,
    fontFamily: fontName,
    fontWeight: FontWeight.w300,
  );

  static const chartTextStyleSpec = charts.TextStyleSpec(
    fontSize: 14,
    color: charts.MaterialPalette.black,
    fontFamily: fontName,
  );
}
