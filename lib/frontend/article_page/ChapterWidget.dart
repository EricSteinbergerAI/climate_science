import 'package:climate_science/backend/authentication.dart';
import 'package:climate_science/backend/content/Chapter.dart';
import 'package:climate_science/backend/content/modules/article/ContentModuleBase.dart';
import 'package:climate_science/backend/content/modules/article/challenges/ChallengeModuleBase.dart';
import 'package:climate_science/frontend/article_page/FeedbackAlert.dart';
import 'package:climate_science/frontend/common/Constants.dart';
import 'package:climate_science/frontend/common/buttons.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import '../article_page/modules/_ModuleBuilderWidget.dart';

class ChapterWidget extends StatefulWidget {
  final Chapter chapter;
  Auth auth;

  ChapterWidget({this.chapter, this.auth});

  @override
  State<StatefulWidget> createState() {
    return ChapterWidgetState();
  }
}

class ChapterWidgetState extends State<ChapterWidget> {
  bool _isDownloadingNext = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          widget.chapter.getChapterName(),
          style: Constants.appBarTextStyle,
          textAlign: TextAlign.center,
        ),
      ),
      body: _build(context: context),
      backgroundColor: Colors.white,
    );
  }

  Widget _build({BuildContext context}) {
    final _allChallengesAttempted = _areAllChallengesAttempted();
    return Stack(
      children: <Widget>[
        SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.fromLTRB(Constants.HORIZONTAL_PADDING, 30,
                Constants.HORIZONTAL_PADDING, 0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 10, 0, 40),
                  child: Text(
                    widget.chapter.getChapterName(),
                    style: Constants.articleTitleTextStyle,
                    textAlign: TextAlign.left,
                  ),
                ),
                Column(
                  children: <Widget>[
                    Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: _getVisibleModules()
                            .map(
                              (contentModule) => ModuleBuilderWidget(
                                widget.chapter,
                                contentModule,
                                _rebuild,
                              ),
                            )
                            .toList()),
                    const Divider(),
                    _allChallengesAttempted
                        ? Container()
                        : Column(
                            children: <Widget>[
                              Container(
                                height: 50,
                                child: const Center(
                                  child: const Text(
                                    "Complete challenge before continuing...",
                                    style: const TextStyle(
                                      fontSize: 15.0,
                                      color: Constants.darkInChapter,
                                      fontWeight: FontWeight.w500,
                                      fontFamily: Constants.fontName,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ),
                              const Divider(
                                thickness: 1.5,
                                color: Constants.darkInChapter,
                              ),
                            ],
                          ),
                    Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: _getFirstFewInvisibleModules()
                            .map((contentModule) => ModuleBuilderWidget(
                                  widget.chapter,
                                  contentModule,
                                  _rebuild,
                                  isActive: false,
                                ))
                            .toList()),
                  ],
                ),
                _allChallengesAttempted
                    ? Padding(
                        padding: const EdgeInsets.fromLTRB(0, 30, 0, 30),
                        child: Column(
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                _getFeedbackButton(),
                                Expanded(
                                  child: Container(),
                                ),
                                _getSocialButton(),
                              ],
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                            _isDownloadingNext
                                ? Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.stretch,
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                        const SizedBox(height: 45),
                                        const LinearProgressIndicator(
                                            valueColor:
                                                const AlwaysStoppedAnimation<
                                                        Color>(
                                                    Constants.buttonColor)),
                                        const SizedBox(height: 45),
                                      ])
                                : _getEndOfChapterButton(context: context),
                          ],
                        ),
                      )
                    : Container(),
              ],
            ),
          ),
        ),
      ],
    );
  }

  void _rebuild() {
    setState(() {});
  }

  List<ContentModuleBase> _getVisibleModules() {
    int firstNotAttempted =
        widget.chapter.getModuleList().indexWhere(_isUnattemptedChallenge);
    if (firstNotAttempted == -1) return widget.chapter.getModuleList();
    return widget.chapter.getModuleList().sublist(0, firstNotAttempted + 1);
  }

  List<ContentModuleBase> _getFirstFewInvisibleModules() {
    int firstNotAttempted =
        widget.chapter.getModuleList().indexWhere(_isUnattemptedChallenge);
    if (firstNotAttempted == -1) return [];
    return widget.chapter.getModuleList().sublist(firstNotAttempted + 1, firstNotAttempted + 5);
  }

  bool _areAllChallengesAttempted() {
    return !widget.chapter.getModuleList().any(_isUnattemptedChallenge);
  }

  bool _isUnattemptedChallenge(ContentModuleBase module) {
    if (!(module is ChallengeModuleBase)) return false;
    ChallengeModuleBase challengeModule = module as ChallengeModuleBase;
    return !challengeModule.isAttempted();
  }

  Widget _getEndOfChapterButton({BuildContext context}) {
    if (widget.chapter.levelOfDetail.getChapters().last == widget.chapter) {
      // Always assumed
      return buildQuizButton(
        context: context,
        quiz: widget.chapter.levelOfDetail.getQuiz(),
        afterPressed: () {
          setState(() {
            _isDownloadingNext = true;
          });
        },
      );
    } else {
      return buildNextChapterButton(
        context: context,
        currentChapter: widget.chapter,
        auth: widget.auth,
        afterPressed: () {
          setState(() {
            _isDownloadingNext = true;
          });
        },
      );
    }
  }

  Widget _getFeedbackButton() {
    return InkWell(
      onTap: () async {
        await showDialog(
          context: context,
          builder: (BuildContext context) {
            return FeedbackAlertDialog(
              auth: widget.auth,
              chapter: widget.chapter,
            );
          },
        );
      },
      splashColor: Colors.white54,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          const Icon(
            Icons.chat_bubble_outline,
            color: Colors.blueAccent,
          ),
          Text("Give Feedback",
              style: Constants.articleBodyTextStyle
                  .copyWith(color: Colors.blueAccent)),
        ],
      ),
    );
  }

  Widget _getSocialButton() {
    return InkWell(
      onTap: () {},
      splashColor: Colors.white54,
      child: Column(
        children: <Widget>[
          const Icon(
            Icons.share,
            color: Colors.blueAccent,
          ),
          Text(
            "Share",
            style: Constants.articleBodyTextStyle
                .copyWith(color: Colors.blueAccent),
          ),
        ],
      ),
    );
  }
}
