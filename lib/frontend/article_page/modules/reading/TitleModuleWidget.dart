import 'package:climate_science/backend/content/modules/article/TitleModule.dart';
import 'package:climate_science/frontend/common/Constants.dart';
import 'package:flutter/material.dart';

class TitleModuleWidget extends StatelessWidget {
  final TitleModule _titleModule;
  final bool isActive;

  TitleModuleWidget(this._titleModule, {this.isActive = true});

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.only(bottom: 15, top: 30),
        child: isActive
            ? Text(
                _titleModule.getText(),
                style: Constants.articleSectionTitleStyle,
                textAlign: TextAlign.left,
              )
            : LayoutBuilder(
                builder: (context, constraints) {
                  return Container(
                    height: Constants.articleSectionTitleStyle.fontSize,
                    width: constraints.constrainWidth() / 3,
                    color: Constants.inactiveColor,
                  );
                },
              ));
  }
}
