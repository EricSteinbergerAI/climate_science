import 'package:climate_science/backend/authentication.dart';
import 'package:climate_science/frontend/common/dismissableAlert.dart';
import 'package:climate_science/frontend/login/theme.dart';
import 'package:flutter/material.dart';

class PasswordResetAlertDialog extends StatefulWidget {
  Auth auth;

  PasswordResetAlertDialog({
    this.auth,
  });

  @override
  _PasswordResetAlertDialogState createState() =>
      _PasswordResetAlertDialogState();
}

class _PasswordResetAlertDialogState extends State<PasswordResetAlertDialog> {
  final _formKey = GlobalKey<FormState>();
  String _resetPasswordEmail;
  String _errorMessage;
  bool _loading;

  @override
  void initState() {
    _resetPasswordEmail = "";
    _errorMessage = "";
    _loading = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return getGeneralAlert(
      context: context,
      body: Form(
        key: _formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisSize: MainAxisSize.min,
          children: _loading
              ? <Widget>[
                  const SizedBox(height: 45),
                  LinearProgressIndicator(
                    valueColor: AlwaysStoppedAnimation<Color>(Colors.white54),
                  ),
                  const SizedBox(height: 10),
                ]
              : <Widget>[
                  ThemeHolder.getTextField(
                    isEmail: true,
                    hintText: "Email",
                    errorText: "Please enter your email",
                    icon: Icons.person,
                    onSaved: (value) => _resetPasswordEmail = value,
                  ),
                  const SizedBox(height: 10),
                  const Text(
                    "We will send you an email to reset your password.",
                    style: ThemeHolder.formTextStyle,
                    maxLines: 55,
                  ),
                  const SizedBox(height: 45),
                  _showErrorMessage(),
                ],
        ),
      ),
      onTap: _loading ? () {} : validateAndSubmit,
      dismissText: _loading ? "Sending email..." : "Send",
    );
  }

  bool validateAndSave() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void validateAndSubmit() async {
    setState(() {
      _errorMessage = "";
    });
    if (validateAndSave()) {
      try {
        FocusScope.of(context).unfocus();
        setState(() {
          _loading = true;
        });
        await widget.auth.resetPassword(_resetPasswordEmail);
        setState(() {
          _loading = false;
        });
        Navigator.pop(context);
      } catch (e) {
        print('Error: $e');
        setState(() {
          try {
            // might fail if error has no code, thus try.
            switch (e.code) {
              case "ERROR_INVALID_EMAIL":
              case "ERROR_USER_NOT_FOUND":
                _errorMessage = "Email not found.";
                break;
              case "ERROR_USER_DISABLED":
                _errorMessage = "Your account has been disabled.";
                break;
              case "ERROR_TOO_MANY_REQUESTS":
              case "ERROR_OPERATION_NOT_ALLOWED":
                _errorMessage =
                    "Our login servers are currently out of service.";
                break;
              default:
                _errorMessage = e.message;
                break;
            }
          } catch (e2) {
            _errorMessage = e.message;
          }
          _loading = false;
          _formKey.currentState.reset();
        });
      }
    }
  }

  Widget _showErrorMessage() {
    if (_errorMessage != null && _errorMessage.length > 0) {
      return Padding(
        padding: const EdgeInsets.fromLTRB(0, 25, 0, 0),
        child: Center(
          child: Text(
            _errorMessage,
            style: ThemeHolder.errorStyle,
            textAlign: TextAlign.center,
            maxLines: null,
          ),
        ),
      );
    } else {
      return Container(
        height: 0.0,
      );
    }
  }
}
