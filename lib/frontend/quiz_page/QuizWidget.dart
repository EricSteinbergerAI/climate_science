import 'package:climate_science/backend/content/CourseQuiz.dart';
import 'package:climate_science/backend/content/modules/quiz/QuizModuleBase.dart';
import 'package:climate_science/frontend/common/Constants.dart';
import 'package:climate_science/frontend/common/GradientProgressWidget.dart';
import 'package:climate_science/frontend/common/buttons.dart';
import 'package:flutter/material.dart';

import 'QuizModuleBaseWidget.dart';

class QuizWidget extends StatefulWidget {
  final CourseQuiz _quiz;

  @override
  QuizWidget(this._quiz);

  @override
  _QuizWidgetState createState() => _QuizWidgetState();
}

class _QuizWidgetState extends State<QuizWidget> {
  List<QuizModuleBase> _modules;
  QuizModuleBase _currentQuestion;

  @override
  initState() {
    super.initState();
    _modules = widget._quiz.moduleList;
    this._currentQuestion = _modules[0];
    for (final m in _modules) {
      m.resetState();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget._quiz.getTitle()),
      ),
      body: SingleChildScrollView(
          child: Container(
              padding: EdgeInsets.fromLTRB(Constants.HORIZONTAL_PADDING, 9,
                  Constants.HORIZONTAL_PADDING, 15),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 2, 0, 5),
                    child: const Text(
                      "Quiz Progress",
                      style: const TextStyle(
                        fontSize: 15.0,
                        color: Constants.darkText,
                        fontFamily: Constants.fontName,
                        fontWeight: FontWeight.w500,
                      ),
                      textAlign: TextAlign.left,
                    ),
                  ),
                  GradientProgressWidget(
                    value: widget._quiz.getProgress(),
                    height: 5,
                    backgroundColor: Colors.grey[200],
                    isRounded: true,
                    blackLineTop: false,
                  ),
                  QuizModuleBaseWidget(_currentQuestion),
                  if (_currentQuestion.isChecked()) _buildFeedbackMsg(),
                  _buildQuizNextButton(),
                ],
              ))),
    );
  }

  Widget _buildQuizNextButton() {
    if (!_currentQuestion.isChecked()) {
      return getButton(
        btnText: "CHECK",
        onPressed: () {
          // if no answer given yet, button does nothing
          if (_currentQuestion.attemptIsComplete()) {
            widget._quiz.registerSolved(
              question: _currentQuestion,
            );
            setState(() {});
          }
        },
      );
    } else {
      if (_currentQuestion == _modules.last) {
        return buildGoHomeButton(
          context: context,
          topic: widget._quiz.topic,
          afterPressed: () {}, // not needed
        );
      }

      return getButton(
        btnText: "CONTINUE",
        onPressed: () {
          setState(() {
            _currentQuestion = _modules[_modules.indexOf(_currentQuestion) + 1];
          });
        },
      );
    }
  }

  Widget _buildFeedbackMsg() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(10, 0, 10, 50),
      child: Column(
        children: <Widget>[
          const Divider(),
          const Padding(
            padding: const EdgeInsets.fromLTRB(0, 10, 0, 12),
            child: const Text(
              "Explanation",
              style: Constants.articleSectionTitleStyle,
            ),
          ),
          Text(
            _currentQuestion.getFeedbackMsg(),
            style: Constants.articleBodyTextStyle,
          ),
        ],
      ),
    );
  }
}
