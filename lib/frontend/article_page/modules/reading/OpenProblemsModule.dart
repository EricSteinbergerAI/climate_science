import 'package:climate_science/backend/content/modules/article/OpenProblemsModule.dart';
import 'package:climate_science/frontend/common/Constants.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class OpenProblemsModuleWidget extends StatelessWidget {
  final OpenProblemsModule _module;
  final bool isActive;

  OpenProblemsModuleWidget(this._module, {this.isActive = true});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 7.0, right: 7, top: 10, bottom: 10),
      child: Container(
        constraints: BoxConstraints(maxWidth: double.infinity),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(6),
          color: Colors.white,
          boxShadow: [
            BoxShadow(color: Constants.shadow, spreadRadius: 2, blurRadius: 5)
          ],
        ),
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: LayoutBuilder(
            builder: (BuildContext context, BoxConstraints constraints) {
              return ExpandablePanel(
                header: Column(
                  children: <Widget>[
                    Text(
                      _module.title,
                      style: Constants.articleBodyBoldTextStyle,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                  ],
                ),
                collapsed: Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: Column(
                    children: <Widget>[
                      ...getTopSection(constraints:constraints),
                      Text(
                        _module.description,
                        softWrap: true,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: Constants.articleBodyTextStyle,
                      ),
                    ],
                  ),
                ),
                expanded: Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      ...getTopSection(constraints:constraints),
                      Text(
                        _module.description,
                        softWrap: true,
                        style: Constants.articleBodyTextStyle,
                      ),
                      Divider(
                        height: 30,
                      ),
                      Text(
                        "Click to learn more:",
                        style: Constants.articleBodySmallBoldTextStyle,
                        textAlign: TextAlign.start,
                      ),
                      ..._module.furtherReading
                          .map(
                            (x) => Padding(
                              padding: const EdgeInsets.only(top: 4),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  const Text(
                                    '→ ',
                                    textAlign: TextAlign.left,
                                    style: Constants.articleBodyLinkTextStyle,
                                  ),
                                  Expanded(
                                    child: RichText(
                                      maxLines: 3,
                                      overflow: TextOverflow.ellipsis,
                                      textAlign: TextAlign.start,
                                      text: TextSpan(
                                        text: x["name"],
                                        style:
                                            Constants.articleBodyLinkTextStyle,
                                        recognizer: new TapGestureRecognizer()
                                          ..onTap = () {
                                            launch(x["link"]);
                                          },
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          )
                          .toList(),
                    ],
                  ),
                ),
                tapHeaderToExpand: true,
                tapBodyToCollapse: true,
                hasIcon: true,
              );
            },
          ),
        ),
      ),
    );
  }

  List getTopSection({constraints}) {
    double W = constraints.maxWidth / 3.02;
    return [
      Container(
        width: double.infinity,
        child: Text(
          "What is needed",
          style: Constants.articleBodySmallBoldTextStyle,
          textAlign: TextAlign.start,
        ),
      ),
      SizedBox(
        height: 8,
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          getIcons(iconData: Icons.build, n: _module.brainScore, width: W),
          getIcons(
              iconData: Icons.attach_money, n: _module.moneyScore, width: W),
          getIcons(iconData: Icons.person, n: _module.societyScore, width: W),
        ],
      ),
      SizedBox(
        height: 4,
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          getCategory(text: "People", width: W),
          getCategory(text: "Money", width: W),
          getCategory(text: "Awareness", width: W),
        ],
      ),
      SizedBox(
        height: 15,
      ),
      Container(
        width: double.infinity,
        child: Text(
          "Description",
          style: Constants.articleBodySmallBoldTextStyle,
          textAlign: TextAlign.start,
        ),
      ),
      SizedBox(
        height: 2,
      ),
    ];
  }

  Widget getCategory({String text, double width}) {
    return Container(
      width: width,
      child: Text(
        text,
        style: Constants.articleBodySmallTextStyle,
        textAlign: TextAlign.center,
      ),
    );
  }

  Widget getIcons({IconData iconData, int n, double width}) {
    return Container(
      width: width,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          for (var i = 0; i < n; i += 1)
            Icon(iconData, color: Constants.inChapter, size: 17.0)
        ],
      ),
    );
  }
}
