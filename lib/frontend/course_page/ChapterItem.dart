import 'package:climate_science/backend/authentication.dart';
import 'package:climate_science/backend/content/Chapter.dart';
import 'package:climate_science/frontend/article_page/ChapterWidget.dart';
import 'package:climate_science/frontend/common/Constants.dart';
import 'package:climate_science/frontend/common/progressbars.dart';
import 'package:climate_science/frontend/common/try_download.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class ChapterItem extends StatefulWidget {
  final Chapter chapter;
  final Auth auth;

  ChapterItem({this.chapter, this.auth});

  @override
  _ChapterItemState createState() => _ChapterItemState();
}

class _ChapterItemState extends State<ChapterItem> {
  @override
  Widget build(BuildContext context) {
    Function maybeGoToChapter = () async {
      if (widget.chapter.levelOfDetail
              .getChapters()
              .any((e) => e.isDownloading) ||
          widget.chapter.levelOfDetail.getQuiz().isDownloading) {
        return;
      }
      if (await Downloading.doNothingIfLoadedElseUpdateAndLoad(
          context: context,
          downloadable: widget.chapter,
          setStateCallback: () => setState(() {}))) {
        widget.chapter.registerChapterOpened();
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => ChapterWidget(
              chapter: widget.chapter,
              auth: widget.auth,
            ),
          ),
        );
      }
      ;
    };
    return InkWell(
      onTap: maybeGoToChapter,
      child: Padding(
        padding: const EdgeInsets.only(top: 2, bottom: 2),
        child: Container(
          height: 65,
          width: double.infinity,
          padding:
              const EdgeInsets.only(right: 3.0, top: 5, bottom: 5, left: 0.0),
          child: Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                child: widget.chapter.isDownloading
                    ? Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: getCircularProgress())
                    : widget.chapter.chapterCoverImg,
              ),
              Expanded(
                child: Text(
                  widget.chapter.getChapterName(),
                  style: Constants.coursePageChapterNameTextStyle,
                  overflow: TextOverflow.ellipsis,
                  softWrap: false,
                  maxLines: 2,
                ),
              ),
              IconButton(
                onPressed: maybeGoToChapter,
                padding: const EdgeInsets.all(0),
                icon: widget.chapter.isCompleted()
                    ? const Icon(
                        Icons.check_circle,
                        color: Constants.grey,
                        size: 23.0,
                      )
                    : const Icon(Icons.keyboard_arrow_right,
                        color: Constants.grey, size: 23.0),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
