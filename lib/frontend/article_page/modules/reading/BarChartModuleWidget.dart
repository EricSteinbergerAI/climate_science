import 'package:charts_flutter/flutter.dart' as charts;
import 'package:climate_science/backend/content/modules/article/BarChartModule.dart';
import 'package:climate_science/frontend/common/Constants.dart';
import 'package:flutter/material.dart';

class BarChartModuleWidget extends StatelessWidget {
  final BarChartModule _module;
  final List<charts.Series<DataPoint, String>> _seriesList;
  final bool isActive;

  final List<DataPoint> fakeData = [
    DataPoint(" ", 1),
    DataPoint("  ", 2),
    DataPoint("   ", 3),
    DataPoint("    ", 3.4),
    DataPoint("     ", 4.1),
  ];

  BarChartModuleWidget(this._module, {this.isActive = true})
      : this._seriesList = [
          charts.Series<DataPoint, String>(
              id: 'Sales',
              colorFn: (_, i) => _getChartColor(
                    color: Constants.inChapter,
                    id: i.toDouble(),
                    n: _module.content.length.toDouble(),
                  ),
              domainFn: (DataPoint t, _) => t.label,
              measureFn: (DataPoint t, _) => t.value,
              data: _module.content,
              labelAccessorFn: (DataPoint t, _) => '${t.value}')
        ];

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(top: 5, bottom: 25),
        height: 340,
        child: isActive ? _buildActive() : _buildInactive());
  }

  Widget _buildActive() {
    return charts.BarChart(
      _seriesList,
      barRendererDecorator: charts.BarLabelDecorator<String>(),
      animate: true,
      domainAxis: charts.OrdinalAxisSpec(
        renderSpec: charts.SmallTickRendererSpec(
          labelStyle: Constants.chartTextStyleSpec,
          labelRotation: 45,
        ),
      ),
      behaviors: [
        charts.ChartTitle(_module.yAxisLabel,
            behaviorPosition: charts.BehaviorPosition.start,
            maxWidthStrategy: charts.MaxWidthStrategy.truncate,
            titleStyleSpec: Constants.chartTextStyleSpec,
            titleOutsideJustification:
                charts.OutsideJustification.middleDrawArea),
      ],
    );
  }

  Widget _buildInactive() {
    return charts.BarChart(
      _getFakeSeriesList(),
      barRendererDecorator: charts.BarLabelDecorator<String>(),
      animate: true,
      domainAxis: charts.OrdinalAxisSpec(
        renderSpec: charts.SmallTickRendererSpec(
          labelStyle: Constants.chartTextStyleSpec,
          labelRotation: 45,
        ),
      ),
    );
  }

  List<charts.Series<DataPoint, String>> _getFakeSeriesList() {
    return [
      charts.Series<DataPoint, String>(
          id: 'Sales',
          colorFn: (_, __) => charts.Color(
              r: Constants.inactiveColor.red,
              g: Constants.inactiveColor.green,
              b: Constants.inactiveColor.blue,
              a: Constants.inactiveColor.alpha),
          domainFn: (DataPoint t, _) => t.label,
          measureFn: (DataPoint t, _) => t.value,
          data: fakeData,
          labelAccessorFn: (DataPoint t, _) => '${t.value}')
    ];
  }

  static charts.Color _getChartColor({Color color, double id, double n}) {
    return charts.Color(
      r: color.red.toInt(),
      g: color.green.toInt(),
      b: color.blue.toInt(),
      a: color.alpha,
    );
  }
}
