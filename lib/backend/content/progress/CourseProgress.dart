import 'package:climate_science/backend/content/Chapter.dart';
import 'package:climate_science/backend/content/Course.dart';

import '../LevelOfDetail.dart';

class CoursePrgress {
  Course _course;

  CoursePrgress({Course course}) {
    this._course = course;
  }

  double getProgress() {
    double bestProgress = -1;

    for (LevelOfDetail lod in this._course.getLODs().values) {
      List<Chapter> chapters = lod.getChapters();
      int all = chapters.length + 1; // +1 is for final quiz
      int done =
          chapters.where((chapter) => chapter.isCompleted()).toList().length;
      if (lod.getQuiz().isCompleted()) {
        ++done; // final quiz
      }

      double lodProgress = done / all;
      if (lodProgress > bestProgress) {
        bestProgress = lodProgress;
      }
    }
    return bestProgress;
  }

  bool isCompleted() {
    return this.getProgress() == 1;
  }
}
