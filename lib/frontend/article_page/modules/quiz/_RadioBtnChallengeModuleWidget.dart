import 'package:climate_science/backend/content/modules/article/challenges/RadioBtnChallengeModule.dart';
import 'package:climate_science/frontend/common/Constants.dart';
import 'package:flutter/material.dart';

class RadioBtnChallengeModuleWidget extends StatefulWidget {
  final RadioBtnChallengeModule content;
  final Function _rebuildCallback;
  final bool isActive;

  RadioBtnChallengeModuleWidget(this.content, this._rebuildCallback,
      {this.isActive = true});

  @override
  State<StatefulWidget> createState() {
    return RadioBtnChallengeModuleWidgetState();
  }
}

class RadioBtnChallengeModuleWidgetState
    extends State<RadioBtnChallengeModuleWidget> {
  void _handleValueChanged(String value) {
    widget.content.registerGuess(value);
    setState(() {});
    widget._rebuildCallback();
  }

  @override
  Widget build(BuildContext context) {
    if (!widget.isActive) return Container();
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              widget.content.getQuestion(),
              style: Constants.articleQuestionStyle,
            ),
            Column(
                children: widget.content
                    .getOptions()
                    .map((value) => Row(children: [
                          Radio(
                            value: value,
                            groupValue: widget.content.getGuess(),
                            onChanged: widget.content.isChecked()
                                ? null
                                : _handleValueChanged,
                            activeColor: Constants.inChapter,
                          ),
                          Text(
                            value,
                            style: Constants.articleBodyTextStyle,
                          )
                        ]))
                    .toList()),
            Padding(
              padding: const EdgeInsets.only(top:9, bottom: 10),
              child: Center(
                child: _printCorrectAnswer(),
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 7, 15, 35),
              child: Align(
                alignment: Alignment.centerRight,
                child: InkWell(
                  child: Text(
                    'Retry?',
                    style: Constants.articleBodyTextStyle
                        .copyWith(color: Constants.lightText),
                  ),
                  onTap: () {
                    widget.content.reset();
                    setState(() {});
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _printCorrectAnswer() {
    if (widget.content.isChecked()) {
      if (widget.content.getCorrectAnswer() == widget.content.getGuess())
        return const Text(
          'Correct!',
          style: Constants.articleFeedbackCorrectTextStyle,
          textAlign: TextAlign.center,
        );
      else
        return Text(
          'Nope, the correct answer is: ${widget.content.getCorrectAnswer()}',
          style: Constants.articleFeedbackWrongTextStyle,
          textAlign: TextAlign.center,
        );
    } else {
      return Text('');
    }
  }
}
