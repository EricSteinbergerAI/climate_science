import 'package:flutter/material.dart';
import 'package:share/share.dart';

class SharingModuleWidget extends StatelessWidget {
  final String text = "Something exciting is happening soon...";
  final String subject = "Something exciting";

  SharingModuleWidget();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Text(
          text,
          style: TextStyle(fontFamily: 'WorkSans'),
        ),
        Align(
          alignment: Alignment.centerRight,
          child: FlatButton(
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text("Share"),
                  Icon(Icons.share),
                ],
              ),
              onPressed: () {
                final RenderBox box = context.findRenderObject();
                Share.share(text,
                    subject: subject,
                    sharePositionOrigin:
                        box.localToGlobal(Offset.zero) & box.size);
              }),
        )
      ],
    );
  }
}
