import UIKit
import Flutter
import Firebase

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {
//    var directory: String!
//    #if DEBUG
//        directory = "Debug"
//    #else
//        directory = "Release"
//    #endif
//    let filePath = Bundle.main.path(forResource: “GoogleService-Info”, ofType: “plist”, inDirectory: directory)
//    let options = FirebaseOptions(contentsOfFile: filePath)!
//    FirebaseApp.configure(options: options)
    FirebaseApp.configure()
    GeneratedPluginRegistrant.register(with: self)
    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
}
