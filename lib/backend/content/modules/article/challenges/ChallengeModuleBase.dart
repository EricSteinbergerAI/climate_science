import 'package:climate_science/backend/content/modules/article/ContentModuleBase.dart';

class ChallengeModuleBase extends ContentModuleBase {
  // Checked and attempted differ in that checked can be reset after the first
  // try, checked can't.
  bool _attempted;
  bool _checked;
  String _moduleId;

  ChallengeModuleBase(String moduleId) {
    this._moduleId = moduleId;
    this._attempted = false;
    this._checked = false;
  }

  void registerAttempted() {
    if (!_checked) {
      this._attempted = true;
      this._checked = true;
      this.store();
    }
  }

  void reset() {
    if (_checked) {
      this._checked = false;
      this.store();
    }
  }

  void store() {
    // Should store answers to challenge
    throw UnsupportedError("not implemented");
  }

  void load() {
    // Should load answers to challenge
    throw UnsupportedError("not implemented");
  }

  bool isAttempted() {
    return this._attempted;
  }

  bool isChecked() {
    return this._checked;
  }

  Map<String, dynamic> getStateAsMap() {
    return {
      "attempted": _attempted,
      "checked": _checked,
    };
  }

  void loadStateFromMap(Map<String, dynamic> stateAsMap) {
    _attempted = stateAsMap["attempted"];
    _checked = stateAsMap["checked"];
  }

  String getFilePath() {
    return '${this.chapter.getDir()}/${this._moduleId}.json';
  }
}
