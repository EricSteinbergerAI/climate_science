import 'ContentModuleBase.dart';

class ParagraphModule extends ContentModuleBase {
  List<RefOrTextBase> _text; // content is either a Str

  ParagraphModule({String content}) {
    this._text = this._decodeText(content: content);
  }

  List<RefOrTextBase> getContent() {
    return this._text;
  }

  List<RefOrTextBase> _decodeText({
    String content,
  }) {
    List<RefOrTextBase> output = new List<RefOrTextBase>();

    int L = content.length;
    String _textBlock = "";
    bool _lastWasSpace = false;
    for (int i = 0; i < L; i++) {
      String s = content[i];

      // references start
      if (s == "[") {
        if (_lastWasSpace) {
          _textBlock = _textBlock.substring(0, _textBlock.length - 1);
          _lastWasSpace = false;
        }
        output.add(TextSubModule(text: _textBlock));
        _textBlock = "";

        List<String> refNames = List<String>();

        String _s = content[++i];
        int j = i;
        while (_s != "]") {
          String refName = "";

          while (_s == " " || _s == ",") {
            _s = content[++j];
          }

          while (_s != "," && _s != "]") {
            refName += _s;
            _s = content[++j];
          }

          if (refName.length > 0) {
            refNames.add(refName);
          }
        }
        i = j;
        output.add(RefsSubModule(refNames: refNames));
      }

      // text continues
      else {
        _textBlock += s;
        _lastWasSpace = s == " ";
      }
    }
    if (_textBlock.length > 0) {
      output.add(TextSubModule(text: _textBlock));
    }
    return output;
  }
}

class TextSubModule extends RefOrTextBase {
  String _text;
  String TYPE = "text";

  TextSubModule({String text}) {
    this._text = text;
  }

  String getText() {
    return _text;
  }
}

class RefsSubModule extends RefOrTextBase {
  List<String> _refNames;
  String TYPE = "ref";

  RefsSubModule({List<String> refNames}) {
    this._refNames = refNames;
  }

  List<String> getRefNames() {
    return this._refNames;
  }
}

class RefOrTextBase {
  String TYPE;
}
