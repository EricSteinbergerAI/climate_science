import 'Chapter.dart';
import 'Course.dart';
import 'CourseQuiz.dart';
import 'Topic.dart';

class LevelOfDetail {
  List<Chapter> _chapterList; // first index is level of detail
  CourseQuiz _quiz;
  String _str;

  Course course;
  Topic topic;

  static const BASICS = "Basics";
  static const ADVANCED = "Details";

  static const BASICS_DISPLAY = "Basics";
  static const ADVANCED_DISPLAY = "Advanced";

  LevelOfDetail({
    List<Chapter> chapterList,
    CourseQuiz courseQuiz,
    String lodAsStr,
  }) {
    this._chapterList = chapterList;
    this._quiz = courseQuiz;
    this._str = lodAsStr;
    assert(lodAsStr == BASICS || lodAsStr == ADVANCED);

    for (var c in chapterList) {
      c.setLevelOfDetail(levelOfDetail: this);
    }
    _quiz.setLevelOfDetail(levelOfDetail: this);
  }

  Future updateFromRemote() async {
    await Future.wait(_chapterList.map((c) => c.updateFromRemote()).toList());
    await this._quiz.updateFromRemote();
  }

  Future init() async {
    for (var c in _chapterList) {
      c.setLevelOfDetail(levelOfDetail: this);
    }
    _quiz.setLevelOfDetail(levelOfDetail: this);
    await Future.wait(_chapterList.map((c) => c.init()).toList());
    await this._quiz.init();
  }

  CourseQuiz getQuiz() {
    return this._quiz;
  }

  List<Chapter> getChapters() {
    return this._chapterList;
  }

  String getOtherStr() {
    if (this._str == LevelOfDetail.BASICS) {
      return LevelOfDetail.ADVANCED;
    }
    return LevelOfDetail.BASICS;
  }

  String asStr() {
    return _str;
  }

  void setTopic({topic}) {
    this.topic = topic;
    for (var c in _chapterList) {
      c.setTopic(topic: topic);
    }
    _quiz.setTopic(topic: topic);
  }

  void setCourse({course}) {
    this.course = course;
    for (Chapter c in _chapterList) {
      c.setCourse(course: course);
    }
    _quiz.setCourse(course: course);
  }
}
