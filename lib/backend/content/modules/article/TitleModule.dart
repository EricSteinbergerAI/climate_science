import 'ContentModuleBase.dart';

class TitleModule extends ContentModuleBase {
  String _text;

  TitleModule({String text}) {
    this._text = text;
  }

  String getText() {
    return this._text;
  }
}
